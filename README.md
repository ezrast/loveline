# loveline

A command line HTTP API client. Currently a work in progress.

## Installation

Clone, `shards build`, and go.

## Usage

See `loveline --help`.

## Contributing

1. Fork it (<https://gitlab.com/ezrast/loveline/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Ezra Stevens](https://gitlab.com/ezrast) - creator and maintainer
