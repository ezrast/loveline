require "./spec_helper"

describe Loveline::CmdCookieCreate do
  Harness.it "creates cookies" do |ll|
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars temperature 375)

    cookies = ll.config.cookie_jar.as(Loveline::DirectoryCookieJar).get_all
    cookies.size.should eq 1
    cookie = cookies.first

    cookie.domain.should eq "foo.example.org"
    cookie.path.should eq "/some/path_with:specialchars"
    cookie.name.should eq "temperature"
    cookie.value.should eq "375"
    cookie.secure.should be_false
  end

  Harness.it "treates an empty path like /" do |ll|
    ll.go %w(cookie create foo.example.org temperature 400 -s)

    cookies = ll.config.cookie_jar.as(Loveline::DirectoryCookieJar).get_all
    cookies.size.should eq 1
    cookie = cookies.first

    cookie.domain.should eq "foo.example.org"
    cookie.path.should eq "/"
    cookie.name.should eq "temperature"
    cookie.value.should eq "400"
    cookie.secure.should be_true
  end
end
