require "./spec_helper"

describe Loveline::CmdCookieDelete do
  Harness.it "deletes cookies" do |ll|
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create bar.example.org temperature 425)

    ll.go %w(cookie delete / -r)
    output = ll.go %w(cookie list -r)
    output.should eq <<-OUTPUT
      No cookies found

      OUTPUT
  end

  Harness.it "produces nice output" do |ll|
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create bar.example.org/x temperature 425)
    ll.go %w(cookie create bar.example.org size small)

    output = ll.go %w(cookie delete bar.example.org -r)
    output.should eq <<-OUTPUT
      Deleting bar.example.org/ size...
      Deleting bar.example.org/x temperature...

      OUTPUT
  end

  Harness.it "deletes named cookies" do |ll|
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create foo.example.org/some temperature 425)
    ll.go %w(cookie create foo.example.org temperature 425)
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars size small)
    ll.go %w(cookie create foo.example.org/some size medium)
    ll.go %w(cookie create foo.example.org size large)

    ll.go %w(cookie delete /some temperature -r)
    output = ll.go %w(cookie list -r)
    output.should eq <<-OUTPUT
      foo.example.org/ size: large
      foo.example.org/ temperature: 425
      foo.example.org/some size: medium
      foo.example.org/some/path_with:specialchars size: small

      OUTPUT
  end

  Harness.it "deletes domain and path matches" do |ll|
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create foo.example.org/some temperature 375)
    ll.go %w(cookie create foo.example.org temperature 425)
    ll.go %w(cookie create bar.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create bar.example.org/some/ temperature 375)
    ll.go %w(cookie create bar.example.org temperature 425)
    ll.go %w(cookie create example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create example.org/somepath temperature 375)
    ll.go %w(cookie create example.org temperature 425)
    ll.go %w(cookie create anexample.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create anexample.org temperature 425)
    ll.go %w(cookie create org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create org temperature 425)

    ll.go %w(cookie delete example.org/some -r)
    output = ll.go %w(cookie list -r)
    output.should eq <<-OUTPUT
      org/ temperature: 425
      org/some/path_with:specialchars temperature: 375
      anexample.org/ temperature: 425
      anexample.org/some/path_with:specialchars temperature: 375
      example.org/ temperature: 425
      example.org/somepath temperature: 375
      bar.example.org/ temperature: 425
      foo.example.org/ temperature: 425

      OUTPUT
  end

  Harness.it "deletes exact domain matches" do |ll|
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create foo.example.org/some temperature 375)
    ll.go %w(cookie create foo.example.org temperature 425)
    ll.go %w(cookie create bar.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create bar.example.org/some/ temperature 375)
    ll.go %w(cookie create bar.example.org temperature 425)
    ll.go %w(cookie create example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create example.org/somepath temperature 375)
    ll.go %w(cookie create example.org temperature 425)
    ll.go %w(cookie create anexample.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create anexample.org temperature 425)
    ll.go %w(cookie create org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create org temperature 425)

    ll.go %w(cookie delete example.org/some -dr)
    output = ll.go %w(cookie list -r)
    output.should eq <<-OUTPUT
      org/ temperature: 425
      org/some/path_with:specialchars temperature: 375
      anexample.org/ temperature: 425
      anexample.org/some/path_with:specialchars temperature: 375
      example.org/ temperature: 425
      example.org/somepath temperature: 375
      bar.example.org/ temperature: 425
      bar.example.org/some/ temperature: 375
      bar.example.org/some/path_with:specialchars temperature: 375
      foo.example.org/ temperature: 425
      foo.example.org/some temperature: 375
      foo.example.org/some/path_with:specialchars temperature: 375

      OUTPUT
  end

  Harness.it "deletes exact path matches" do |ll|
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create foo.example.org/some temperature 375)
    ll.go %w(cookie create foo.example.org temperature 425)
    ll.go %w(cookie create bar.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create bar.example.org/some/ temperature 375)
    ll.go %w(cookie create bar.example.org temperature 425)
    ll.go %w(cookie create example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create example.org/somepath temperature 375)
    ll.go %w(cookie create example.org temperature 425)
    ll.go %w(cookie create anexample.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create anexample.org temperature 425)
    ll.go %w(cookie create org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create org temperature 425)

    ll.go %w(cookie delete example.org/some -pr)
    output = ll.go %w(cookie list -r)
    output.should eq <<-OUTPUT
      org/ temperature: 425
      org/some/path_with:specialchars temperature: 375
      anexample.org/ temperature: 425
      anexample.org/some/path_with:specialchars temperature: 375
      example.org/ temperature: 425
      example.org/some/path_with:specialchars temperature: 375
      example.org/somepath temperature: 375
      bar.example.org/ temperature: 425
      bar.example.org/some/ temperature: 375
      bar.example.org/some/path_with:specialchars temperature: 375
      foo.example.org/ temperature: 425
      foo.example.org/some/path_with:specialchars temperature: 375

      OUTPUT
  end

  Harness.it "deletes request-style matches" do |ll|
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create foo.example.org/some temperature 375)
    ll.go %w(cookie create foo.example.org temperature 425)
    ll.go %w(cookie create bar.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create bar.example.org/some/ temperature 375)
    ll.go %w(cookie create bar.example.org temperature 425)
    ll.go %w(cookie create example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create example.org/somepath temperature 375)
    ll.go %w(cookie create example.org temperature 425)
    ll.go %w(cookie create anexample.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create anexample.org temperature 425)
    ll.go %w(cookie create org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create org temperature 425)

    ll.go %w(cookie delete example.org/some)
    output = ll.go %w(cookie list -r)
    output.should eq <<-OUTPUT
      org/some/path_with:specialchars temperature: 375
      anexample.org/ temperature: 425
      anexample.org/some/path_with:specialchars temperature: 375
      example.org/some/path_with:specialchars temperature: 375
      example.org/somepath temperature: 375
      bar.example.org/ temperature: 425
      bar.example.org/some/ temperature: 375
      bar.example.org/some/path_with:specialchars temperature: 375
      foo.example.org/ temperature: 425
      foo.example.org/some temperature: 375
      foo.example.org/some/path_with:specialchars temperature: 375

      OUTPUT
  end

  Harness.it "prioritizes -d and -p over -r" do |ll|
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create foo.example.org/some temperature 375)
    ll.go %w(cookie create foo.example.org temperature 425)
    ll.go %w(cookie create bar.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create bar.example.org/some/ temperature 375)
    ll.go %w(cookie create bar.example.org temperature 425)
    ll.go %w(cookie create example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create example.org/somepath temperature 375)
    ll.go %w(cookie create example.org temperature 425)
    ll.go %w(cookie create anexample.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create anexample.org temperature 425)
    ll.go %w(cookie create org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create org temperature 425)

    ll.go %w(cookie delete example.org -dpr)
    output = ll.go %w(cookie list -r)
    output.should eq <<-OUTPUT
      org/ temperature: 425
      org/some/path_with:specialchars temperature: 375
      anexample.org/ temperature: 425
      anexample.org/some/path_with:specialchars temperature: 375
      example.org/some/path_with:specialchars temperature: 375
      example.org/somepath temperature: 375
      bar.example.org/ temperature: 425
      bar.example.org/some/ temperature: 375
      bar.example.org/some/path_with:specialchars temperature: 375
      foo.example.org/ temperature: 425
      foo.example.org/some temperature: 375
      foo.example.org/some/path_with:specialchars temperature: 375

      OUTPUT
  end

  Harness.it "prints something nice when there are no cookies" do |ll|
    output = ll.go %w(cookie delete /)
    output.should eq <<-OUTPUT
      No cookies found

      OUTPUT
  end

  Harness.it "prints something nice when there is no cookie jar" do |ll|
    ll.raw_config.cookie_jar = :None
    output = ll.go %w(cookie delete /)
    output.should eq <<-OUTPUT
      Cookie jar is not writeable

      OUTPUT
  end
end
