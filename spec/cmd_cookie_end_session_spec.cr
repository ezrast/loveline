require "./spec_helper"

describe Loveline::CmdCookieEndSession do
  Harness.it "deletes non-persistent cookies" do |ll|
    ll.go_exec ["exec", "-MGET", "-A#{LOCAL_ADDR}set_cookies/style=gingersnap"]
    ll.go_exec ["exec", "-MGET", "-A#{LOCAL_ADDR}set_cookies/sweetener=molasses", "-Hmax_age=9999"]
    ll.go_exec ["exec", "-MGET", "-A#{LOCAL_ADDR}set_cookies/bake_time=12m"]
    ll.go_exec ["exec", "-MGET", "-A#{LOCAL_ADDR}set_cookies/baker=kofi", "-Hmax_age=9999"]
    ll.go %w(cookie end-session)

    output = ll.go %w(cookie list -r)
    output.should eq <<-OUTPUT
      #{LOCAL_IP}/ baker: kofi
      #{LOCAL_IP}/ sweetener: molasses

      OUTPUT
  end
end
