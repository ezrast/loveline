require "./spec_helper"

describe Loveline::CmdCookieList do
  Harness.it "lists cookies" do |ll|
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create bar.example.org temperature 425)

    output = ll.go %w(cookie list -r)
    output.should eq <<-OUTPUT
      bar.example.org/ temperature: 425
      foo.example.org/some/path_with:specialchars temperature: 375

      OUTPUT
  end

  Harness.it "lists named cookies" do |ll|
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create foo.example.org/some temperature 425)
    ll.go %w(cookie create foo.example.org temperature 425)
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars size small)
    ll.go %w(cookie create foo.example.org/some size medium)
    ll.go %w(cookie create foo.example.org size large)

    output = ll.go %w(cookie list /some temperature -r)
    output.should eq <<-OUTPUT
      foo.example.org/some temperature: 425
      foo.example.org/some/path_with:specialchars temperature: 375

      OUTPUT
  end

  Harness.it "lists domain and path matches" do |ll|
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create foo.example.org/some temperature 375)
    ll.go %w(cookie create foo.example.org temperature 425)
    ll.go %w(cookie create bar.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create bar.example.org/some/ temperature 375)
    ll.go %w(cookie create bar.example.org temperature 425)
    ll.go %w(cookie create example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create example.org/somepath temperature 375)
    ll.go %w(cookie create example.org temperature 425)
    ll.go %w(cookie create anexample.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create anexample.org temperature 425)
    ll.go %w(cookie create org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create org temperature 425)

    output = ll.go %w(cookie list example.org/some -r)
    output.should eq <<-OUTPUT
      example.org/some/path_with:specialchars temperature: 375
      bar.example.org/some/ temperature: 375
      bar.example.org/some/path_with:specialchars temperature: 375
      foo.example.org/some temperature: 375
      foo.example.org/some/path_with:specialchars temperature: 375

      OUTPUT
  end

  Harness.it "lists exact domain matches" do |ll|
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create foo.example.org/some temperature 375)
    ll.go %w(cookie create foo.example.org temperature 425)
    ll.go %w(cookie create bar.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create bar.example.org/some/ temperature 375)
    ll.go %w(cookie create bar.example.org temperature 425)
    ll.go %w(cookie create example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create example.org/somepath temperature 375)
    ll.go %w(cookie create example.org temperature 425)
    ll.go %w(cookie create anexample.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create anexample.org temperature 425)
    ll.go %w(cookie create org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create org temperature 425)

    output = ll.go %w(cookie list example.org/some -dr)
    output.should eq <<-OUTPUT
      example.org/some/path_with:specialchars temperature: 375

      OUTPUT
  end

  Harness.it "lists exact path matches" do |ll|
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create foo.example.org/some temperature 375)
    ll.go %w(cookie create foo.example.org temperature 425)
    ll.go %w(cookie create bar.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create bar.example.org/some/ temperature 375)
    ll.go %w(cookie create bar.example.org temperature 425)
    ll.go %w(cookie create example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create example.org/somepath temperature 375)
    ll.go %w(cookie create example.org temperature 425)
    ll.go %w(cookie create anexample.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create anexample.org temperature 425)
    ll.go %w(cookie create org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create org temperature 425)

    output = ll.go %w(cookie list example.org/some -pr)
    output.should eq <<-OUTPUT
      foo.example.org/some temperature: 375

      OUTPUT
  end

  Harness.it "lists request-style matches" do |ll|
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create foo.example.org/some temperature 375)
    ll.go %w(cookie create foo.example.org temperature 425)
    ll.go %w(cookie create bar.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create bar.example.org/some/ temperature 375)
    ll.go %w(cookie create bar.example.org temperature 425)
    ll.go %w(cookie create example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create example.org/somepath temperature 375)
    ll.go %w(cookie create example.org temperature 425)
    ll.go %w(cookie create anexample.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create anexample.org temperature 425)
    ll.go %w(cookie create org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create org temperature 425)

    output = ll.go %w(cookie list example.org/some)
    output.should eq <<-OUTPUT
      org/ temperature: 425
      example.org/ temperature: 425

      OUTPUT
  end

  Harness.it "prioritizes -d and -p over -r" do |ll|
    ll.go %w(cookie create foo.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create foo.example.org/some temperature 375)
    ll.go %w(cookie create foo.example.org temperature 425)
    ll.go %w(cookie create bar.example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create bar.example.org/some/ temperature 375)
    ll.go %w(cookie create bar.example.org temperature 425)
    ll.go %w(cookie create example.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create example.org/somepath temperature 375)
    ll.go %w(cookie create example.org temperature 425)
    ll.go %w(cookie create anexample.org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create anexample.org temperature 425)
    ll.go %w(cookie create org/some/path_with:specialchars temperature 375)
    ll.go %w(cookie create org temperature 425)

    output = ll.go %w(cookie list example.org -dpr)
    output.should eq <<-OUTPUT
      example.org/ temperature: 425

      OUTPUT
  end

  Harness.it "prints something nice when there are no cookies" do |ll|
    output = ll.go %w(cookie list /)
    output.should eq <<-OUTPUT
      No cookies found

      OUTPUT
  end

  Harness.it "prints something nice when there is no cookie jar" do |ll|
    ll.raw_config.cookie_jar = :None
    output = ll.go %w(cookie list)
    output.should eq <<-OUTPUT
      No cookie jar configured

      OUTPUT
  end
end
