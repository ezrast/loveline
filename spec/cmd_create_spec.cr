require "./spec_helper"
require "yaml"

describe Loveline::CmdCreate do
  Harness.it "creates empty templates" do |ll|
    ll.go %w(create -n myTemplate)
    template_dir = ll.config.template_dir.value
    File.open("#{template_dir}/myTemplate"){ |ff| YAML.parse(ff) }.should eq(
      {
        "tpl_v1" => nil,
        "address" => nil,
        "method" => nil,
        "headers" => Loveline::Headers.new,
        "cookies" => {} of String => String,
        "variables" => {} of String => String,
      }
    )
  end

  Harness.it "creates full templates" do |ll|
    ll.go %w(create -n myTemplate -A http://localhost/ -M GET -H one=two -H three=four -C flavor=oatmeal -C consistency=chewy -V ant=boar -V cow=dog)
    template_dir = ll.config.template_dir.value
    File.open("#{template_dir}/myTemplate"){ |ff| YAML.parse(ff) }.should eq({
      "tpl_v1" => nil,
      "address" => "http://localhost/",
      "method" => "GET",
      "headers" => [
        ["one", "two"],
        ["three", "four"],
      ],
      "cookies" => {
        "flavor" => "oatmeal",
        "consistency" => "chewy",
      },
      "variables" => {
        "ant" => "boar",
        "cow" => "dog",
      }
    })
  end

  Harness.it "laminates templates" do |ll|
    ll.go %w(create -n tpl1 -M GET -A http://localhost:1234 -C a=1 -C b=2)
    ll.go %w(create -n tpl2 -M HEAD -H foo=bar -C b=3)
    ll.go %w(create -n tpl3 tpl1 tpl2 -H foo=baz)
    template_dir = ll.config.template_dir.value
    File.open("#{template_dir}/tpl3"){ |ff| YAML.parse(ff) }.should eq({
      "tpl_v1" => nil,
      "address" => "http://localhost:1234",
      "method" => "HEAD",
      "headers" => [
        ["foo", "bar"],
        ["foo", "baz"],
      ],
      "cookies" => {
        "a" => "1",
        "b" => "3",
      },
      "variables" => {} of String => String
    })
  end

  Harness.it "won't overwrite a template" do |ll|
    ll.go %w(create -n mytpl -M GET)
    expect_raises(Loveline::TemplateNameCollision) do
      ll.go %w(create -n mytpl -M POST)
    end
    output = ll.go %w(show mytpl)
    output.should eq <<-OUTPUT
      --- mytpl ---
      GET

      OUTPUT
  end

  Harness.it "will overwrite a template with -f" do |ll|
    ll.go %w(create -n mytpl -M GET -f)
    ll.go %w(create -n mytpl -M POST -f)

    output = ll.go %w(show mytpl)
    output.should eq <<-OUTPUT
      --- mytpl ---
      POST

      OUTPUT
  end
end
