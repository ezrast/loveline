require "./spec_helper"

describe Loveline::CmdDelete do
  Harness.it "deletes templates" do |ll|
    ll.go %w(create -n tpl1 -M HEAD)
    ll.go %w(create -n tpl2 -M GET)
    ll.go %w(create -n tpl3 -M POST)
    ll.go %w(create -n tpl4 -M PUT)
    ll.go %w(create -n tpl5 -M DELETE)
    ll.go %w(delete)
    output_1 = ll.go %w(list)

    template_libraries = ll.config.template_libraries.map{ |tl| tl.value }
    template_dir = ll.config.template_dir.value

    output_1.should eq <<-OUTPUT
      In #{template_dir}:
      - tpl1
      - tpl2
      - tpl3
      - tpl4
      - tpl5

      In #{template_libraries[0]}:
        No templates found

      In #{template_libraries[1]}:
        No templates found


      OUTPUT

    ll.go %w(delete tpl2)
    output_2 = ll.go %w(list)
    output_2.should eq <<-OUTPUT
      In #{template_dir}:
      - tpl1
      - tpl3
      - tpl4
      - tpl5

      In #{template_libraries[0]}:
        No templates found

      In #{template_libraries[1]}:
        No templates found


      OUTPUT

    ll.go %w(delete tpl1 tpl5 tpl4)
    output_3 = ll.go %w(list)
    output_3.should eq <<-OUTPUT
      In #{template_dir}:
      - tpl3

      In #{template_libraries[0]}:
        No templates found

      In #{template_libraries[1]}:
        No templates found


      OUTPUT
  end

  Harness.it "prints something appropriate on bad template names" do |ll|
    template_dir = ll.config.template_dir.value
    output = ll.go %w(delete unknown)
    output.should eq <<-OUTPUT
      No template `unknown` found in #{template_dir}\n
      OUTPUT
  end
end
