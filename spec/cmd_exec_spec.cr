require "./spec_helper"

describe Loveline::CmdExec do
  Harness.it "execs saved templates" do |ll|
    ll.go "create -n myTemplate -A #{LOCAL_ADDR}echo -M POST -H reverse=true -B quickbrownfox"
    output = ll.go_exec %w(exec myTemplate)
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 13
      Body:
      xofnworbkciuq

      OUTPUT
  end

  Harness.it "assumes HTTP" do |ll|
    output = ll.go_exec "exec -M POST -A #{LOCAL_IP}:#{LOCAL_PORT}/echo -B hello"
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 5
      Body:
      hello

      OUTPUT
  end

  Harness.it "execs laminate templates" do |ll|
    ll.go "create -n tpl1 -M GET -A #{LOCAL_ADDR}echo -B cestlavie"
    ll.go %w(create -n tpl2 -M POST -H upcase=true)
    output = ll.go_exec %w(exec tpl1 tpl2 -H foo=baz)
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 9
      Body:
      CESTLAVIE

      OUTPUT
  end

  Harness.it "sends the right user agent" do |ll|
    output = ll.go_exec "exec -M GET -A #{LOCAL_ADDR}echo_header/User-Agent"
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: #{Loveline::USER_AGENT.size}
      Body:
      #{Loveline::USER_AGENT}

      OUTPUT
  end

  Harness.it "allows the user agent to be overridden" do |ll|
    output = ll.go_exec "exec -M GET -A #{LOCAL_ADDR}echo_header/User-Agent -H User-Agent=HappyAgent"
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 10
      Body:
      HappyAgent

      OUTPUT
  end

  Harness.it "asks about missing addresses" do |ll|
    expect = [
      {"Enter address: ", "#{LOCAL_ADDR}gizmo"},
    ]
    output = ll.go_exec %w(exec -M FROBNICATE), expect
    output.should eq <<-OUTPUT
      --- 404 NOT FOUND ---
      Headers:
        Content-Length: 0
        Request-Method: FROBNICATE
        Request-Path: /gizmo

      OUTPUT
  end

  Harness.it "defaults method to GET" do |ll|
    output = ll.go_exec "exec -A #{LOCAL_ADDR}blah"
    output.should eq <<-OUTPUT
      --- 404 NOT FOUND ---
      Headers:
        Content-Length: 0
        Request-Method: GET
        Request-Path: /blah

      OUTPUT
  end

  Harness.it "asks about variables" do |ll|
    expect = [
      {"Enter variable `path`: ", "echo_header"},
      {"Enter variable `headername`: ", "drink"},
      {"Enter variable `headervalue`: ", "lemonade"},
    ]
    output = ll.go_exec("exec -M GET -A #{LOCAL_ADDR}%[path]/drink -H %[headername]=%[headervalue]", expect)
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 8
      Body:
      lemonade

      OUTPUT
  end

  Harness.it "raises instead of asking if interactive is false" do |ll|
    expect_raises(Loveline::RequestNoAddress) do
      ll.go(["-o", "interactive: false", "exec", "-M", "GET", "--dry"])
    end

    ll.raw_config.interactive = :False
    expect_raises(Loveline::RequestNoAddress) do
      ll.go %w(exec --dry)
    end
  end

  Harness.it "uses environment variables" do |ll|
    output = ll.go_exec("exec -M GET -A #{LOCAL_ADDR} -B %[inenv]")
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 0
        Request-Body: hello
        Request-Method: GET
        Request-Path: /

      OUTPUT
  end

  Harness.it "uses nested variables" do |ll|
    output = ll.go_exec("exec -M GET -A #{LOCAL_ADDR} -B %[%[var0]] -V var0=%[var1]%[var2] -V var1=foo -V var2=bar -V foobar=goodbye")
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 0
        Request-Body: goodbye
        Request-Method: GET
        Request-Path: /

      OUTPUT
  end

  Harness.it "disallows infinitely nested variables" do |ll|
    expect_raises(Loveline::InterpolationLimitExceeded) do
      ll.go("exec --dry -M GET -A #{LOCAL_ADDR} -B %[var1] -V var1=%[var2] -V var2=%[var1]")
    end
  end

  Harness.it "disallows invalid %-escapes" do |ll|
    expect_raises(Loveline::BadTemplateEscape) do
      ll.go("exec --dry -M GET -A #{LOCAL_ADDR} -B %blah")
    end
  end

  Harness.it "disallows invalid % at EOL" do |ll|
    expect_raises(Loveline::BadTemplateEscape) do
      ll.go("exec --dry -M GET -A #{LOCAL_ADDR} -B %")
    end
  end

  Harness.it "allows escaped literal %" do |ll|
    output = ll.go_exec("exec -M GET -A #{LOCAL_ADDR} -B %%[var0] -V var0=%[var1]%[var2] -V var1=foo -V var2=bar -V foobar=goodbye")
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 0
        Request-Body: %[var0]
        Request-Method: GET
        Request-Path: /

      OUTPUT
  end

  Harness.it "prints info with --dry" do |ll|
    output = ll.go "exec -M GET -A #{LOCAL_ADDR} -V foo=two -H One=%[foo] --dry"
    output.should eq <<-OUTPUT
      --- dry run ---
      GET #{LOCAL_ADDR}
      Headers:
        One: two
        User-Agent: #{Loveline::USER_AGENT}

      OUTPUT
  end

  Harness.it "pipes response bodies into programs (array form)" do |ll|
    output = ll.go_exec "exec -M POST -A #{LOCAL_ADDR}echo -H Content-Type=text/loveline_test_pipe_array -B CoreDump"
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 8
        Content-Type: text/loveline_test_pipe_array
      Piping body to: tr o a
      CareDump
      OUTPUT
  end

  Harness.it "pipes response bodies into programs (string form)" do |ll|
    output = ll.go_exec "exec -M POST -A #{LOCAL_ADDR}echo -H Content-Type=text/loveline_test_pipe_string -B CoreDump"
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 8
        Content-Type: text/loveline_test_pipe_string
      Piping body to: sed s/u/a/g
      CoreDamp
      OUTPUT
  end

  Harness.it "opens response bodies as files (array form)" do |ll|
    output = ll.go_exec "exec -M POST -A #{LOCAL_ADDR}echo -H Content-Type=text/loveline_test_tempfile_array -B one+two+three+four+five+six+"
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 28
        Content-Type: text/loveline_test_tempfile_array
      Opening body with: grep e %s
      one
      three
      five

      OUTPUT
  end

  Harness.it "uses wildcard type handlers" do |ll|
    output = ll.go_exec "-o {'content_type_handlers':{'*':'|sort'}} exec -M POST -A #{LOCAL_ADDR}echo -B one+two+three+four+five+six+"
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 28
      Piping body to: sort
      five
      four
      one
      six
      three
      two

      OUTPUT
  end

  Harness.it "uses specific type handlers over wildcards" do |ll|
    output = ll.go_exec "-o {'content_type_handlers':{'*':'|sort','foo':'|cat'}} exec -M POST -A #{LOCAL_ADDR}echo -H Content-Type=foo -B one+two+three+four+five+six+"
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 28
        Content-Type: foo
      Piping body to: cat
      one
      two
      three
      four
      five
      six

      OUTPUT
  end

  Harness.it "opens response bodies as files (string form)" do |ll|
    output = ll.go_exec "exec -M POST -A #{LOCAL_ADDR}echo -H Content-Type=text/loveline_test_tempfile_string -B one+two+three+four+five+six+"
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 28
        Content-Type: text/loveline_test_tempfile_string
      Opening body with: grep o %s
      one
      two
      four

      OUTPUT
  end

  Harness.it "disallows insecure TLS when not configured" do |ll|
    ll.raw_config.insecure_tls = :False
    expect_raises(Loveline::TlsError) do
      ll.go_exec "exec -M GET -A #{LOCAL_ADDR_HTTPS}", https: true
    end
  end

  #
  # Cookie-related tests below this point
  #
  Harness.it "saves valid cookies" do |ll|
    ll.go_exec ["exec", "-M", "POST", "-A", "#{LOCAL_ADDR}echo", "-H", "Set-Cookie=sweetener=sugar; Http-Only; Expires=Sat, 19 oct 3019 23:51:12 GMT"]

    cookies = ll.config.cookie_jar.as(Loveline::DirectoryCookieJar).get_all
    cookies.size.should eq 1
    cookie = cookies.first

    cookie.domain.should eq LOCAL_IP
    cookie.path.should eq "/echo"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    cookie.expiry.should eq Time.utc(3019, 10, 19, 23, 51, 12)
    cookie.secure.should be_false
    cookie.http_only.should be_false
    cookie.host_only.should be_true
  end

  Harness.it "does not save invalid cookies" do |ll|
    ll.go_exec ["exec", "-M", "POST", "-A", "#{LOCAL_ADDR}echo", "-H", "Set-Cookie=sweetener sugar; Http-Only; Expires=Sat, 19 oct 3019 23:51:12 GMT"]

    cookie_dir = ll.config.cookie_jar.as(Loveline::DirectoryCookieJar).dir
    Dir.exists?(cookie_dir).should be_false
  end

  Harness.it "sends cookies from templates" do |ll|
    output = ll.go_exec "exec -M GET -A #{LOCAL_ADDR}show_cookies -C frosting=cream"
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 15
      Body:
      frosting=cream


      OUTPUT
  end

  Harness.it "saves cookies" do |ll|
    ll.go_exec "exec -M GET -A #{LOCAL_ADDR}set_cookies/sweetener=sugar"

    cookies = ll.config.cookie_jar.as(Loveline::DirectoryCookieJar).get_all
    cookies.size.should eq 1
    cookie = cookies.first

    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    cookie.path.should eq "/"
    cookie.secure.should be_false
    cookie.http_only.should be_false
  end

  Harness.it "sends cookies from cookie jar" do |ll|
    ll.go "cookie create #{LOCAL_IP}/ sweetener sucralose"

    output = ll.go_exec "exec -M GET -A #{LOCAL_ADDR}show_cookies"
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 20
      Body:
      sweetener=sucralose


      OUTPUT
  end

  Harness.it "does not save cookies if cookie_jar is disabled" do |ll|
    ll.raw_config.cookie_jar = :None
    ll.go_exec "exec -M GET -A #{LOCAL_ADDR}/set_cookies/texture=crunchy"

    output = ll.go_exec "exec -M GET -A #{LOCAL_ADDR}show_cookies"
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 0

      OUTPUT
  end

  Harness.it "sends secure cookies via https only" do |ll|
    ll.go_exec "exec -M GET -A #{LOCAL_ADDR_HTTPS}set_cookies/texture=moist -H secure=true", https: true

    output_1 = ll.go_exec "exec -M GET -A #{LOCAL_ADDR}show_cookies"
    output_1.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 0

      OUTPUT

    output_2 = ll.go_exec "exec -M GET -A #{LOCAL_ADDR_HTTPS}show_cookies", https: true
    output_2.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 14
      Body:
      texture=moist


      OUTPUT
  end

  Harness.it "sets secure cookies via https only" do |ll|
    ll.go_exec "exec -M GET -A #{LOCAL_ADDR}set_cookies/frosting=none -H secure=true"
    ll.go_exec "exec -M GET -A #{LOCAL_ADDR_HTTPS}set_cookies/baker=alfonse -H secure=true", https: true

    output = ll.go_exec "exec -M GET -A #{LOCAL_ADDR_HTTPS}show_cookies", https: true
    output.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 14
      Body:
      baker=alfonse


      OUTPUT
  end

  Harness.it "doesn't send expired cookies" do |ll|
    ll.go_exec ["exec", "-MGET", "-A", "#{LOCAL_ADDR}set_cookies/frosting=marzipan", "-H", "expires=#{HTTP.format_time(Time.utc.shift(seconds: 1))}"]
    ll.go_exec ["exec", "-MGET", "-A", "#{LOCAL_ADDR}set_cookies/baker=fyodor", "-H", "max_age=3"]

    output_0s = ll.go_exec "exec -M GET -A #{LOCAL_ADDR}show_cookies"
    output_0s.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 31
      Body:
      frosting=marzipan
      baker=fyodor


      OUTPUT

    sleep 2
    output_2s = ll.go_exec "exec -M GET -A #{LOCAL_ADDR}show_cookies"
    output_2s.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 13
      Body:
      baker=fyodor


      OUTPUT

    sleep 2
    output_4s = ll.go_exec "exec -M GET -A #{LOCAL_ADDR}show_cookies"
    output_4s.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 0

      OUTPUT
  end

  Harness.it "sends cookies only with a matching path" do |ll|
    ll.go_exec "exec -M GET -A #{LOCAL_ADDR}set_cookies/frosting=pumpkin -H path=/"
    ll.go_exec "exec -M GET -A #{LOCAL_ADDR}set_cookies/sweetener=honey -H path=/show_cookies"
    ll.go_exec "exec -M GET -A #{LOCAL_ADDR}set_cookies/baker=janet -H path=/show_cookies/bar"
    ll.go_exec "exec -M GET -A #{LOCAL_ADDR}set_cookies/flavor=oatmeal -H path=/show_cookies/bar/"
    ll.go_exec "exec -M GET -A #{LOCAL_ADDR}set_cookies/texture=chewy -H path=/show_cookies/bar/baz"

    output_1 = ll.go_exec "exec -M GET -A #{LOCAL_ADDR}show_cookies/bar"
    output_1.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 45
      Body:
      frosting=pumpkin
      sweetener=honey
      baker=janet


      OUTPUT

    output_2 = ll.go_exec "exec -M GET -A #{LOCAL_ADDR}show_cookies/bar/"
    output_2.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 60
      Body:
      frosting=pumpkin
      sweetener=honey
      baker=janet
      flavor=oatmeal


      OUTPUT
  end

  Harness.it "sends cookies only with a matching domain" do |ll|
    ll.go "cookie create bar.foo.example.com fat         butter"
    ll.go "cookie create foo.example.com     size        medium"
    ll.go "cookie create foo.example.com -o  servings    24"
    ll.go "cookie create example.com         temperature 350"
    ll.go "cookie create never.example.org   addin       macadamias"

    output_1 = ll.go_exec "exec -M GET -A #{LOCAL_ADDR}show_cookies -H Host=bar.foo.example.com"
    output_1.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 39
      Body:
      fat=butter
      size=medium
      temperature=350


      OUTPUT

    output_2 = ll.go_exec "exec -M GET -A #{LOCAL_ADDR}show_cookies -H Host=foo.example.com"
    output_2.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 40
      Body:
      size=medium
      servings=24
      temperature=350


      OUTPUT

    output_3 = ll.go_exec "exec -M GET -A #{LOCAL_ADDR}show_cookies -H Host=example.com"
    output_3.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 16
      Body:
      temperature=350


      OUTPUT

    output_4 = ll.go_exec "exec -M GET -A #{LOCAL_ADDR}show_cookies -H Host=com"
    output_4.should eq <<-OUTPUT
      --- 200 OK ---
      Headers:
        Content-Length: 0

      OUTPUT
  end

  Harness.it "disallows cross-domain cookie-setting" do |ll|
    ll.go_exec ["exec", "-MGET", "-A#{LOCAL_ADDR}set_cookies/addin=chocolatechip", "-HHost=ah.com",
      "-H", "domain=blah.com"]
    ll.go_exec ["exec", "-MGET", "-A#{LOCAL_ADDR}set_cookies/fat=margarine", "-HHost=com",
      "-H", "domain=blah.com"]
    ll.go_exec ["exec", "-MGET", "-A#{LOCAL_ADDR}set_cookies/flavor=mint", "-HHost=blah.com",
      "-H", "domain=blah.com"]

    output = ll.go "cookie list -r"
    output.should eq "blah.com/ flavor: mint\n"
  end

  Harness.it "allows subdomains to set cookies for superdomains" do |ll|
    ll.go_exec ["exec", "-MGET", "-A#{LOCAL_ADDR}set_cookies/flavor=fudge", "-HHost=hrmph.blah.com",
      "-H", "domain=blah.com"]

    output = ll.go "cookie list -r"
    output.should eq "blah.com/ flavor: fudge\n"
  end

  Harness.it "overwrites existing cookies but keeps their creation time" do |ll|
    ll.go_exec ["exec", "-MGET", "-A#{LOCAL_ADDR}set_cookies/flavor=vanilla"]
    sleep 2
    ll.go_exec ["exec", "-MGET", "-A#{LOCAL_ADDR}set_cookies/flavor=peanutbutter"]

    cookie = ll.config.cookie_jar.load_exact?(LOCAL_IP, "/", "flavor").not_nil!
    cookie.value.should eq "peanutbutter"
    (1.5..2.5).should contain (Time.utc - cookie.creation_time).total_seconds
  end

  Harness.it "raises on bad hostname" do |ll|
    expect_raises(Loveline::HttpExecError) { ll.go "exec -A ..." }
  end

  Harness.it "raises on bad address" do |ll|
    expect_raises(Loveline::AddressParseError) { ll.go "exec -A :::" }
  end
end
