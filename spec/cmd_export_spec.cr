require "./spec_helper"

describe Loveline::CmdExportJinja do
  Harness.it "raises on bad filename" do |ll|
    expect_raises(Loveline::TemplateFileError){ ll.go "export jinja foo -A abc" }
  end
end
