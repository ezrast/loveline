require "./spec_helper"

describe Loveline::CmdList do
  Harness.it "lists templates" do |ll|
    bad_path = "#{Dir.tempdir}/#{rand(UInt64)}"
    template_libraries = ll.config.template_libraries.map{ |tl| tl.value }

    template_libraries << bad_path
    ll.go %w(create -n ABC)
    ll.go %w(create -n DEF)
    output = ll.go %w(list)
    output.should eq <<-OUTPUT
      In #{ll.config.template_dir.value}:
      - ABC
      - DEF

      In #{template_libraries[0]}:
        No templates found

      In #{template_libraries[1]}:
        No templates found


      OUTPUT
  end
end
