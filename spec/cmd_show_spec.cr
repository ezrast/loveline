require "./spec_helper"

describe Loveline::CmdShow do
  Harness.it "shows empty templates" do |ll|
    output = ll.go %w(show)
    output.should eq <<-OUTPUT
      ---  ---

      OUTPUT
  end

  Harness.it "shows saved templates" do |ll|
    ll.go %w(create -n myTemplate -A http://localhost/ -M GET -H one=two -H three=four -V ant=boar -V cow=dog -C recipe_id=1x2y3z -C baker=Roslyn)
    output = ll.go %w(show myTemplate)
    output.should eq <<-OUTPUT
      --- myTemplate ---
      GET http://localhost/
      Headers:
        one: two
        three: four
      Cookies:
        baker: Roslyn
        recipe_id: 1x2y3z
      Variables:
        ant: boar
        cow: dog

      OUTPUT
  end

  Harness.it "shows laminate templates" do |ll|
    ll.go %w(create -n tpl1 -M GET -A http://localhost:1234)
    ll.go %w(create -n tpl2 -M HEAD -H foo=bar)
    output = ll.go %w(show tpl1 tpl2 -H foo=baz -H abc=xyz -H foo=quux)
    output.should eq <<-OUTPUT
      --- tpl1:tpl2:<ephemeral> ---
      HEAD http://localhost:1234
      Headers:
        abc: xyz
        foo: bar
        foo: baz
        foo: quux

      OUTPUT
  end

  Harness.it "treats positionals with :// as if they used -A" do |ll|
    output = ll.go "show http://foo"
    output.should eq <<-OUTPUT
      --- <ephemeral> ---
      ... http://foo

      OUTPUT
  end

  Harness.it "treats positionals with = as if they used -V" do |ll|
    output = ll.go "show foo=bar"
    output.should eq <<-OUTPUT
      --- <ephemeral> ---
      Variables:
        foo: bar

      OUTPUT
  end
end
