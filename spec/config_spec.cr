require "./spec_helper"

describe Enum do
  it "serializes as text" do
    {
      Loveline::Config::Autobool::True,
      Loveline::Config::Autobool::False,
    }.to_yaml.should eq <<-YAML
      ---
      - true
      - false

      YAML
  end

  it "deserializes text" do
    obj = Array(
      Loveline::Config::Autobool |
      Loveline::Config::Autobool
    ).from_yaml <<-YAML
      - FaLsE
      - true
      YAML
    obj.should eq([
      Loveline::Config::Autobool::False,
      Loveline::Config::Autobool::True,
    ])
  end

  it "doesn't deserialize integers" do
    expect_raises(YAML::ParseException) do
      obj = Array(
        Loveline::Config::Autobool |
        Loveline::Config::Autobool
      ).from_yaml <<-YAML
        - 0
        - 1
        YAML
    end
  end
end

describe Loveline::Config do
  it "defaults template libraries to XDG directories" do
    ENV["XDG_DATA_DIRS"] = "/foo:/bar"
    ENV["HOME"] = "/myhome"

    config = Loveline::Config.new(
      Loveline::Config::Raw.from_yaml(""),
      input_tty: false,
      output_tty: false
    )
    config.template_libraries.should eq [
      Loveline::Config::Path.new("/foo/loveline/templates"),
      Loveline::Config::Path.new("/bar/loveline/templates"),
    ]

    ENV.delete "XDG_DATA_DIRS"

    config = Loveline::Config.new(
      Loveline::Config::Raw.from_yaml(""),
      input_tty: false,
      output_tty: false
    )
    config.template_libraries.should eq [
      Loveline::Config::Path.new("/usr/local/share/loveline/templates"),
      Loveline::Config::Path.new("/usr/share/loveline/templates"),
    ]
  end

  it "defaults template directory to XDG directory" do
    ENV["XDG_DATA_HOME"] = "/foo"
    ENV["HOME"] = "/myhome"

    config = Loveline::Config.new(
      Loveline::Config::Raw.from_yaml(""),
      input_tty: false,
      output_tty: false
    )
    config.template_dir.value.should eq "/foo/loveline/templates"

    ENV.delete "XDG_DATA_HOME"

    config = Loveline::Config.new(
      Loveline::Config::Raw.from_yaml(""),
      input_tty: false,
      output_tty: false
    )

    config.template_dir.value.should eq "/myhome/.local/share/loveline/templates"

    ENV.delete "HOME"
  end

  it "defaults cookie jar to XDG directory" do
    ENV["HOME"] = "/myhome"
    begin
      ENV["XDG_DATA_HOME"] = "/foo"
      begin
        config = Loveline::Config.new(
          Loveline::Config::Raw.from_yaml(""),
          input_tty: false,
          output_tty: false
        )
        config.cookie_jar.as(Loveline::DirectoryCookieJar).dir.should eq "/foo/loveline/cookies"
      ensure
        ENV.delete "XDG_DATA_HOME"
      end

      config = Loveline::Config.new(
        Loveline::Config::Raw.from_yaml(""),
        input_tty: false,
        output_tty: false
      )

      config.cookie_jar.as(Loveline::DirectoryCookieJar).dir.should eq "/myhome/.local/share/loveline/cookies"
    ensure
      ENV.delete "HOME"
    end
  end

  Harness.it "prints in color" do |ll|
    output = ll.go(["-o", "color: true", "show"])
    output.should eq "\e[94m---  ---\n\e[0m"

    ll.raw_config.color = :True
    output = ll.go %w(show)
    output.should eq "\e[94m---  ---\n\e[0m"
  end

  Harness.it "raises on bad config" do |ll|
    expect_raises(Loveline::ConfigError) do
      ll.go(["-o", "spam: true", "show"])
    end
  end

  Harness.it "expands ~ in cookie jar paths" do |ll|
    ENV["HOME"] = "/path/to/home"
    begin
      config = Loveline::Config.new(
        Loveline::Config::Raw.from_yaml("cookie_jar: ~/cookies"),
        input_tty: false,
        output_tty: false
      )
      config.cookie_jar.as(Loveline::DirectoryCookieJar).dir.should eq "/path/to/home/cookies"
    ensure
      ENV.delete "HOME"
    end
  end
end
