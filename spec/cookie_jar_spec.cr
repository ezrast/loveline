require "./spec_helper"

describe Loveline::DirectoryCookieJar do
  Harness.it "creates cookie dir as 0o700 and parents as 0o755" do |ll|
    cookie_jar = ll.config.cookie_jar.as Loveline::DirectoryCookieJar

    parent = File.dirname(cookie_jar.dir.to_s)
    Dir.exists?(parent).should be_false

    cookie_jar.save(
      Loveline::Cookie.new(
        domain: "x",
        path: "/",
        name: "a",
        value: "b",
        creation_time: Time.utc(2000, 1, 1),
        last_access_time: Time.utc(2000, 1, 1),
        expiry: nil,
        secure: false,
        http_only: false,
        host_only: true
      ), false
    )

    parent_info = File.info(parent)
    parent_info.permissions.to_i.should eq 0o755
    info = File.info(cookie_jar.dir.to_s)
    info.permissions.to_i.should eq 0o700
  end

  Harness.it "saves cookies as YAML, encoding slashes and colons in path names" do |ll|
    cookie_jar = ll.config.cookie_jar.as(Loveline::DirectoryCookieJar)
    cookie1 = Loveline::Cookie.new(
      domain: "x.y.z",
      path: "/foo:bar/baz:quux:#/",
      name: "a",
      value: "b",
      creation_time: Time.utc(2000, 1, 1),
      last_access_time: Time.utc(2000, 1, 1),
      expiry: nil,
      secure: false,
      http_only: false,
      host_only: true
    )

    cookie_jar.save(cookie1, false)

    cookie2 = File.open("#{cookie_jar.dir}/x.y.z/::foo:.bar::baz:.quux:.#::/a"){ |ff| Loveline::Cookie.from_yaml(ff) }
    cookie2.should eq cookie1
  end

  Harness.it "discriminates between cookies with long paths" do |ll|
    ll.go_exec ["exec", "-M", "POST", "-A", "#{LOCAL_ADDR}echo/l#{"o" * 256}ngpath/1", "-H", "Set-Cookie=long1=x; path=/echo/l#{"o" * 256}ngpath;Http-Only; Expires=Sat, 19 oct 3019 23:51:12 GMT"]
    ll.go_exec ["exec", "-M", "POST", "-A", "#{LOCAL_ADDR}echo/l#{"o" * 256}ngpath/2", "-H", "Set-Cookie=long2=y; path=/echo/l#{"o" * 256}ngpath/2;Http-Only; Expires=Sat, 19 oct 3019 23:51:12 GMT"]
    ll.config.cookie_jar.as(Loveline::DirectoryCookieJar).search("", "/echo/l#{"o" * 256}ngpath", true, Time.utc,
      domain_matcher: Loveline::Cookie::DomainMatcher::Subdomains,
      path_matcher: Loveline::Cookie::PathMatcher::Subpaths
    ).size.should eq 2
    ll.config.cookie_jar.as(Loveline::DirectoryCookieJar).search("", "/echo/l#{"o" * 256}ngpath/1", true, Time.utc,
      domain_matcher: Loveline::Cookie::DomainMatcher::Subdomains,
      path_matcher: Loveline::Cookie::PathMatcher::Subpaths
    ).size.should eq 0
    ll.config.cookie_jar.as(Loveline::DirectoryCookieJar).search("", "/echo/l#{"o" * 256}ngpath/2", true, Time.utc,
      domain_matcher: Loveline::Cookie::DomainMatcher::Subdomains,
      path_matcher: Loveline::Cookie::PathMatcher::Subpaths
    ).size.should eq 1
    ll.config.cookie_jar.as(Loveline::DirectoryCookieJar).search("", "/echo/l#{"o" * 256}ngpath/2", true, Time.utc,
      domain_matcher: Loveline::Cookie::DomainMatcher::Subdomains,
      path_matcher: Loveline::Cookie::PathMatcher::Request
    ).size.should eq 2
  end
end
