require "./spec_helper"

describe Loveline::Cookie do
  it "parses simple cookies" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.domain.should eq "bar.foo.org"
    cookie.path.should eq "/some/path"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    (0..2).should contain (Time.utc - cookie.creation_time).total_seconds
    (0..2).should contain (Time.utc - cookie.last_access_time).total_seconds
    cookie.expiry.should be_nil
    cookie.secure.should be_false
    cookie.http_only.should be_false
    cookie.host_only.should be_true
  end

  it "rejects cookies with no equals sign" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener sugar", "bar.foo.org", "/some/path", false
    cookie.should be_nil
  end

  it "only consumes the first equals sign" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar=carbs", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.domain.should eq "bar.foo.org"
    cookie.path.should eq "/some/path"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar=carbs"
    (0..2).should contain (Time.utc - cookie.creation_time).total_seconds
    (0..2).should contain (Time.utc - cookie.last_access_time).total_seconds
    cookie.expiry.should be_nil
    cookie.secure.should be_false
    cookie.http_only.should be_false
    cookie.host_only.should be_true
  end

  it "rejects cookies with no name" do
    cookie = Loveline::Cookie.parse_set_cookie_string "=sugar", "bar.foo.org", "/some/path", false
    cookie.should be_nil
  end

  it "parses cookies with no value" do
    cookie = Loveline::Cookie.parse_set_cookie_string "umame=", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.name.should eq "umame"
    cookie.value.should be_empty
  end

  it "parses cookies with an Expires attribute" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Expires=Sat, 19 oct 2019 23:51:12 GMT", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.domain.should eq "bar.foo.org"
    cookie.path.should eq "/some/path"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    (0..2).should contain (Time.utc - cookie.creation_time).total_seconds
    (0..2).should contain (Time.utc - cookie.last_access_time).total_seconds
    cookie.expiry.should eq Time.utc(2019, 10, 19, 23, 51, 12)
    cookie.secure.should be_false
    cookie.http_only.should be_false
    cookie.host_only.should be_true
  end

  it "ignores unparseable expiries" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Expires=yolo", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.domain.should eq "bar.foo.org"
    cookie.path.should eq "/some/path"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    (0..2).should contain (Time.utc - cookie.creation_time).total_seconds
    (0..2).should contain (Time.utc - cookie.last_access_time).total_seconds
    cookie.expiry.should be_nil
    cookie.secure.should be_false
    cookie.http_only.should be_false
    cookie.host_only.should be_true
  end

  it "parses cookies with a Max-Age attribute" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Max-Age=100", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.domain.should eq "bar.foo.org"
    cookie.path.should eq "/some/path"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    (0..2).should contain (Time.utc - cookie.creation_time).total_seconds
    (0..2).should contain (Time.utc - cookie.last_access_time).total_seconds
    (98..100).should contain (cookie.expiry.not_nil! - Time.utc).total_seconds
    cookie.secure.should be_false
    cookie.http_only.should be_false
    cookie.host_only.should be_true
  end

  it "turns negative Max-Ages into the epoch" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Max-Age=-123", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.domain.should eq "bar.foo.org"
    cookie.path.should eq "/some/path"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    (0..2).should contain (Time.utc - cookie.creation_time).total_seconds
    (0..2).should contain (Time.utc - cookie.last_access_time).total_seconds
    cookie.expiry.should eq Time::UNIX_EPOCH
    cookie.secure.should be_false
    cookie.http_only.should be_false
    cookie.host_only.should be_true
  end

  it "ignores unparseable Max-Ages" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Max-Age=one million", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.domain.should eq "bar.foo.org"
    cookie.path.should eq "/some/path"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    (0..2).should contain (Time.utc - cookie.creation_time).total_seconds
    (0..2).should contain (Time.utc - cookie.last_access_time).total_seconds
    cookie.expiry.should be_nil
    cookie.secure.should be_false
    cookie.http_only.should be_false
    cookie.host_only.should be_true
  end

  it "prioritizes Max-Age over Expires" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Max-Age=100; Expires=Sat, 19 oct 3019 23:51:12 GMT", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    (98..100).should contain (cookie.expiry.not_nil! - Time.utc).total_seconds
  end

  it "prioritizes Max-Age over Expires" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Expires=Sat, 19 oct 3019 23:51:12 GMT; Max-Age=100", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    (98..100).should contain (cookie.expiry.not_nil! - Time.utc).total_seconds
  end

  it "parses cookies with a Domain attribute" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Domain=foo.org", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.domain.should eq "foo.org"
    cookie.path.should eq "/some/path"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    (0..2).should contain (Time.utc - cookie.creation_time).total_seconds
    (0..2).should contain (Time.utc - cookie.last_access_time).total_seconds
    cookie.expiry.should be_nil
    cookie.secure.should be_false
    cookie.http_only.should be_false
    cookie.host_only.should be_false
  end

  it "ignores a leading period in the Domain attribute" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Domain=.foo.org", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.domain.should eq "foo.org"
    cookie.path.should eq "/some/path"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    (0..2).should contain (Time.utc - cookie.creation_time).total_seconds
    (0..2).should contain (Time.utc - cookie.last_access_time).total_seconds
    cookie.expiry.should be_nil
    cookie.secure.should be_false
    cookie.http_only.should be_false
    cookie.host_only.should be_false
  end

  it "converts Domains to lower case" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Domain=BAR.Foo.oRG", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.domain.should eq "bar.foo.org"
    cookie.path.should eq "/some/path"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    (0..2).should contain (Time.utc - cookie.creation_time).total_seconds
    (0..2).should contain (Time.utc - cookie.last_access_time).total_seconds
    cookie.expiry.should be_nil
    cookie.secure.should be_false
    cookie.http_only.should be_false
    cookie.host_only.should be_false
  end

  it "rejects too-specific Domains" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Domain=baz.bar.foo.org", "bar.foo.org", "/some/path", false
    cookie.should be_nil
  end

  it "rejects Domains that don't match the request" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Domain=.mail.foo.org", "bar.foo.org", "/some/path", false
    cookie.should be_nil
  end

  it "rejects Domains that don't match the request" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sessionid=totallylegit; Domain=bank.com", "bar.foo.org", "/some/path", false
    cookie.should be_nil
  end

  it "rejects Domains when the host is an IPv4 address" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Domain=2.3.4", "1.2.3.4", "/some/path", false
    cookie.should be_nil
  end

  it "rejects Domains when the host is an IPv6 address" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Domain=3.4", "1::2.3.4.5", "/some/path", false
    cookie.should be_nil
  end

  it "parses cookies with a Path attribute" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Path=/different", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.domain.should eq "bar.foo.org"
    cookie.path.should eq "/different"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    (0..2).should contain (Time.utc - cookie.creation_time).total_seconds
    (0..2).should contain (Time.utc - cookie.last_access_time).total_seconds
    cookie.expiry.should be_nil
    cookie.secure.should be_false
    cookie.http_only.should be_false
    cookie.host_only.should be_true
  end

  it "clobbers Paths that don't begin with /" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Path=invalid", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.domain.should eq "bar.foo.org"
    cookie.path.should eq "/some/path"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    (0..2).should contain (Time.utc - cookie.creation_time).total_seconds
    (0..2).should contain (Time.utc - cookie.last_access_time).total_seconds
    cookie.expiry.should be_nil
    cookie.secure.should be_false
    cookie.http_only.should be_false
    cookie.host_only.should be_true
  end

  it "clobbers default paths that don't begin with /" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Path=invalid", "bar.foo.org", "some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.domain.should eq "bar.foo.org"
    cookie.path.should eq "/"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    (0..2).should contain (Time.utc - cookie.creation_time).total_seconds
    (0..2).should contain (Time.utc - cookie.last_access_time).total_seconds
    cookie.expiry.should be_nil
    cookie.secure.should be_false
    cookie.http_only.should be_false
    cookie.host_only.should be_true
  end

  it "chomps trailing slashes from the default path" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Path=invalid", "bar.foo.org", "/some/path/", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.domain.should eq "bar.foo.org"
    cookie.path.should eq "/some/path"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    (0..2).should contain (Time.utc - cookie.creation_time).total_seconds
    (0..2).should contain (Time.utc - cookie.last_access_time).total_seconds
    cookie.expiry.should be_nil
    cookie.secure.should be_false
    cookie.http_only.should be_false
    cookie.host_only.should be_true
  end

  it "parses cookies with a Secure attribute if the connection is secure" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Secure", "bar.foo.org", "/some/path", true
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.domain.should eq "bar.foo.org"
    cookie.path.should eq "/some/path"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    (0..2).should contain (Time.utc - cookie.creation_time).total_seconds
    (0..2).should contain (Time.utc - cookie.last_access_time).total_seconds
    cookie.expiry.should be_nil
    cookie.secure.should be_true
    cookie.http_only.should be_false
    cookie.host_only.should be_true
  end

  it "rejects cookies with a Secure attribute if the connection is not secure" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Secure", "bar.foo.org", "/some/path", false
    cookie.should be_nil
  end

  it "parses cookies with an HttpOnly attribute" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; HttpOnly", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.domain.should eq "bar.foo.org"
    cookie.path.should eq "/some/path"
    cookie.name.should eq "sweetener"
    cookie.value.should eq "sugar"
    (0..2).should contain (Time.utc - cookie.creation_time).total_seconds
    (0..2).should contain (Time.utc - cookie.last_access_time).total_seconds
    cookie.expiry.should be_nil
    cookie.secure.should be_false
    cookie.http_only.should be_true
    cookie.host_only.should be_true
  end

  it "prioritizes later attributes over earlier ones" do
    cookie = Loveline::Cookie.parse_set_cookie_string "sweetener=sugar; Path=/alpha; Max-Age=10; Max-Age=30; Path=/beta", "bar.foo.org", "/some/path", false
    cookie.should_not be_nil
    cookie = cookie.not_nil!
    cookie.path.should eq "/beta"
    (28..30).should contain (cookie.expiry.not_nil! - Time.utc).total_seconds
  end

  {
    "foo!bar",
    "",
    "example dot org",
    ":::1",
    "::q",
    "1::2::3",
  }.each do |str|
    it "raises on nonsense domains: #{str}" do
      expect_raises(Loveline::BadCookieDomain) do
        Loveline::Cookie.new(
          domain: str,
          path: "/",
          name: "a",
          value: "b",
          creation_time: Time.utc(2000, 1, 1),
          last_access_time: Time.utc(2000, 1, 1),
          expiry: nil,
          secure: false,
          http_only: false,
          host_only: true
        )
      end
    end
  end

  {
    "1.2.3.4",
    "::1",
    "abcd::",
    "1::2.3.4.5",
  }.each do |str|
    it "allows IP addresses instead of domains: #{str}" do
      Loveline::Cookie.new(
        domain: str,
        path: "/",
        name: "a",
        value: "b",
        creation_time: Time.utc(2000, 1, 1),
        last_access_time: Time.utc(2000, 1, 1),
        expiry: nil,
        secure: false,
        http_only: false,
        host_only: true
      ).domain.should eq str
    end
  end

  it "raises on empty names" do
    expect_raises(Loveline::EmptyCookieName) do
      Loveline::Cookie.new(
        domain: "foo.com",
        path: "/",
        name: "",
        value: "b",
        creation_time: Time.utc(2000, 1, 1),
        last_access_time: Time.utc(2000, 1, 1),
        expiry: nil,
        secure: false,
        http_only: false,
        host_only: true
      )
    end
  end

  {
    {"Sat, 19 oct 2019 23:51:12 GMT", Time.utc(2019, 10, 19, 23, 51, 12)},
    {"Sat, 19 oct 70 23:51:12 GMT", Time.utc(1970, 10, 19, 23, 51, 12)},
    # Cookie dates do *not* support time zones despite printing GMT in
    # serialization
    {"Sat, 19 oct 2019 23:51:12 PDT", Time.utc(2019, 10, 19, 23, 51, 12)},
    {"Sat, 19 oct 2019 23:51:12 GMT - Sun, 20 oct 2019 10:00:00 GMT", Time.utc(2019, 10, 19, 23, 51, 12)},
    {"2019 jun 04 1:2:3", Time.utc(2019, 6, 4, 1, 2, 3)},
    {"jan 11 01:00:00 1700", Time.utc(1700, 1, 11, 1, 0, 0)},
    {"~*~ 10 69 09:32:01 DeCeMbEr ~*~", Time.utc(2069, 12, 10, 9, 32, 1)},
  }.each do |pair|
    it "parses `#{pair.first}` to a Time" do
      Loveline::Cookie.parse_date(pair.first).should eq pair.last
    end
  end

  {
    "Sat, 19 okt 2019 23:51:12 GMT",
    "Sat, 1900 oct 2019 23:51:12 GMT",
    "Sat, 19 oct 1219 23:51:12 GMT",
    "Sat, 32 oct 2019 23:51:12 GMT",
    "Sat, 19 oct 2019 25:51:12 GMT",
    "Sat, 19 oct 2019 23:81:12 GMT",
    "Sat, 19 oct 2019 23:51:92 GMT",
    "Sat, 19 oct 2019 23:51:120 GMT",
    "Sat, 2019-10-19 23:51:12 GMT",
    "Sat, nineteenth of oct 2019 23:51:12 GMT",
    "Sat, 19 oct o19 23:51:12 GMT",
    "Sat, 19 oct 2019 23.51.12 GMT",
    "19 oct 2019",
    "",
  }.each do |str|
    it "does not parse #{str} to a Time" do
      Loveline::Cookie.parse_date(str).should be_nil
    end
  end
end
