require "./spec_helper"

INTERP = Loveline::Interpolator.new(
  {
    "foo" => "bar",
    "bar" => "%[baz]",
    "baz" => "quux",
    "barrel" => "monkeys",
    "recurse" => "%[recurse]",
    "inenv" => "never_looks_here",
  },
  ->(var_name : String){ "unset-#{rand(Int64)}".as String? }
)

describe Loveline::Interpolator do
  it "parses interpolatable variables" do
    INTERP["%[%[foo]rel]"].should eq "monkeys"
    INTERP["%[bar]"].should eq "quux"
    INTERP["%V[bar]"].should eq "quux"
  end

  it "asks once about unset variables" do
    x1 = INTERP["%[x]"]
    y = INTERP["%[y]"]
    x2 = INTERP["%[x]"]

    y.should start_with "unset"
    x1.should start_with "unset"
    x1.should_not eq y
    x2.should eq x1
  end

  it "parses literal variables" do
    INTERP["%v[bar]"].should eq "%[baz]"
  end

  it "looks to environment variables first" do
    INTERP["%[inenv]"].should eq "hello"
  end

  it "includes interpolatable files" do
    tempfile = File.tempfile("loveline-test") { |ff| ff << "%[foo]" }
    begin
      INTERP["%F[#{tempfile.path}]"].should eq "bar"
    ensure
      tempfile.delete
    end
  end

  it "includes literal files" do
    tempfile = File.tempfile("loveline-test") { |ff| ff << "%[foo]" }
    begin
      INTERP["%f[#{tempfile.path}]"].should eq "%[foo]"
    ensure
      tempfile.delete
    end
  end

  it "allows literal %'s" do
    INTERP["%%[foo]"].should eq "%[foo]"
    INTERP["%%%[foo]"].should eq "%bar"
  end

  it "raises on recursion" do
    expect_raises(Loveline::InterpolationLimitExceeded) do
      INTERP["%[recurse]"]
    end
  end

  [
    "%",
    "%x[foo]",
    "%fblah",
    "%%%",
    "%[foo",
  ].each do |str|
    it "raises on bad escapes: #{str}" do
      expect_raises(Loveline::BadTemplateEscape){ INTERP[str] }
    end
  end
end
