require "file_utils"
require "spec"

require "../src/loveline/loveline"

ENV["LOVELINE_VAR_inenv"] = "hello"
ENV.delete "HOME"

LOCAL_IP = "127.0.0.60"
LOCAL_PORT = 60000
LOCAL_ADDR = "http://#{LOCAL_IP}:#{LOCAL_PORT}/"
LOCAL_ADDR_HTTPS = "https://#{LOCAL_IP}:#{LOCAL_PORT}/"

class Harness
  getter raw_config : Loveline::Config::Raw

  def config(*, input_tty = true, output_tty = true)
    Loveline::Config.new(@raw_config, input_tty: input_tty, output_tty: output_tty)
  end

  def self.it(test_desc : String, &block : Proc(Harness, Nil))
    ::it test_desc do
      temp_dir_path = "#{Dir.tempdir}/lovelinetest.#{Time.utc.to_unix}.#{rand(UInt32)}"
      Dir.mkdir(temp_dir_path)
      begin
        template_dir_1 = "#{temp_dir_path}/tpl1"
        Dir.mkdir(template_dir_1)
        template_dir_2 = "#{temp_dir_path}/tpl2"
        Dir.mkdir(template_dir_2)
        template_dir_3 = "#{temp_dir_path}/tpl3"
        Dir.mkdir(template_dir_3)
        cookie_dir = "#{temp_dir_path}/sub_dir/cookies"

        raw_config = Loveline::Config::Raw.from_yaml <<-YAML
          template_libraries:
          - #{template_dir_1}
          - #{template_dir_2}
          template_dir: #{template_dir_3}
          cookie_jar: #{cookie_dir}
          content_type_handlers:
            text/loveline_test_pipe_array: ["|tr", "o", "a"]
            text/loveline_test_pipe_string: "|sed s/u/a/g"
            text/loveline_test_tempfile_array: ["grep", "e", "%s"]
            text/loveline_test_tempfile_string: "grep o %s"
          color: false
          truncate_body_after: 1024
          interactive: true
          insecure_tls: true

          YAML

        block.call(new(raw_config))
      ensure
        FileUtils.rm_r(temp_dir_path)
      end
    end
  end

  private def initialize(@raw_config)
  end

  enum Status
    Timeout
    Complete
  end

  def go(command_line : String, *args, **kwargs)
    go(command_line.split, *args, **kwargs)
  end

  def go(command_line : Array(String), expectations = [] of {String, String}) : String
    IO.pipe do |input_r, input_w|
      IO.pipe do |output_r, output_w|
        chan = Channel(Char | Exception | Status).new
        spawn do
          begin
            Loveline.new(command_line, @raw_config, input: input_r, output: output_w).go
          rescue err : Exception
            chan.send(err)
          ensure
            input_r.close
            output_w.close
          end
        end

        spawn do
          sleep 2
          chan.send Status::Timeout
          output_w.close
        end

        spawn do
          begin
            while exp = expectations.shift?
              expect(exp[0], exp[1], output_r, input_w)
            end
            while char = output_r.read_char
              chan.send char
            end
            chan.send Status::Complete
          rescue err
            chan.send(err)
          end
        end

        ret = String::Builder.new
        while true
          case msg = chan.receive
          when Exception
            raise msg
          when Status::Timeout
            error_msg = msg.to_s
            if error_msg.empty?
              raise "Timeout!"
            else
              raise "Timeout! Output so far: `#{msg.to_s}`"
            end
          when Char
            ret << msg
          when Status::Complete
            break
          end
        end
        return ret.to_s
      end
    end
  end

  def expect(expected, answer, rr, ww)
    chars = expected.each_char
    so_far = String::Builder.new
    until (next_expected = chars.next).is_a? Iterator::Stop
      unless next_expected == rr.read_char.tap{ |cc| so_far << cc }
        rr.peek.try{ |bytes| so_far << String.new(bytes) }
        # always fails since we're actually checking char-by-char
        so_far.to_s.should eq expected
        break # never executes
      end
    end
    ww.puts(answer)
  end

  def go_exec(command_line : String, *args, **kwargs)
    go_exec(command_line.split, *args, **kwargs)
  end

  def go_exec(command_line : Array(String), expectations = [] of {String, String}, https = false) : String
    chan = Channel(Bool).new(1)

    server = TestServer.new(TestHandler.new(chan))

    begin
      if https
        tls_context = OpenSSL::SSL::Context::Server.from_hash({
          "key" => "spec/cert/loveline-test.key",
          "cert" => "spec/cert/loveline-test.crt",
        })
        server.bind_tls LOCAL_IP, LOCAL_PORT, tls_context
      else
        server.bind_tcp LOCAL_IP, LOCAL_PORT
      end

      spawn do
        server.listen
      end

      spawn do
        sleep 2
        chan.send(false)
      end

      Fiber.yield
      ret = go(command_line, expectations)
    ensure
      server.close
    end

    result = chan.receive

    unless result
      raise "Timeout"
    end
    return ret
  end
end

class TestHandler
  include HTTP::Handler
  def call(context)
    req = context.request
    resp = context.response
    resp.headers.delete "Connection"

    body = req.body.try(&.gets_to_end).to_s

    case {req.method, req.path}
    when {"POST", %r[\A/echo(/\S*)?]} #/
      body = body.tr("+", "\n")
      body = body.upcase if req.headers["upcase"]? == "true"
      body = body.reverse if req.headers["reverse"]? == "true"

      if content_type = req.headers["Content-Type"]?
        resp.headers["Content-Type"] = content_type
      end
      if set_cookie = req.headers["Set-Cookie"]?
        resp.headers["Set-Cookie"] = set_cookie
      end

      resp << body
    when {"GET", "/"}
      resp.headers["Request-Method"] = req.method
      resp.headers["Request-Path"] = req.path
      resp.headers["Request-Body"] = body unless body.empty?
    when {"GET", %r[\A/echo_header/(.+)\z]}
      resp << req.headers[$1]?
    when {"GET", .starts_with?("/show_cookies")}
      HTTP::Cookies.from_headers(req.headers).each do |cookie|
        resp << cookie.name << "=" << cookie.value << "\n"
      end
    when {"GET", %r[\A/set_cookies(/\w+?=\w+?)+\z]} #/
      cookies = HTTP::Cookies.new
      secure = (req.headers["secure"]? == "true")
      path = req.headers["path"]? || "/"
      domain = req.headers["domain"]?
      expires = req.headers["expires"]?
      max_age = req.headers["max_age"]?

      $1.lstrip('/').split('/').each do |pair|
        name, _, value = pair.partition('=')
        cookies << HTTP::Cookie.new(
          name: name,
          value: value,
          path: path,
          expires: (expires && Time::Format::HTTP_DATE.parse(expires)) || (max_age && Time.utc + max_age.to_i.seconds),
          domain: domain,
          secure: secure,
        )
      end
      cookies.add_response_headers(resp.headers)
    else
      resp.headers["Request-Method"] = req.method
      resp.headers["Request-Path"] = req.path
      resp.headers["Request-Body"] = body unless body.empty?

      resp.status = HTTP::Status::NOT_FOUND
    end

    @chan.send(true)
  end

  def initialize(@chan : Channel(Bool))
  end
end

class TestServer < HTTP::Server
  # default behavior is to dump the error to STDERR, but that pollutes
  # terminal output when we test for TLS connection errors
  private def handle_exception(e : Exception)
  end
end
