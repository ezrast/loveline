require "./spec_helper"

describe Loveline::AnonTemplate do
  {
    '^', '(', ')', '<', '>', '@', ',', ';', ':', '\\', '"', '/', '[',
    ']', '?', '{', '}', ' ', '\t', '='
  }.each do |char|
    it "rejects cookie names with #{char}" do
      expect_raises(Loveline::BadCookieName) do
        Loveline::AnonTemplate.new(
          address: nil,
          method: nil,

          headers: Loveline::Headers.new,
          cookies: {"cookie#{char}name" => "value"},
          variables: {} of String => String,

          body: nil,
        )
      end
    end
  end
end
