require "colorize"

require "./loveline/loveline"

begin
  Loveline.new.go
rescue err : Loveline::LovelineException
  STDERR.puts err
  exit 1
rescue err
  STDERR.puts "=== Error ===".colorize.light_red
  STDERR.puts <<-EOS
    #{err.message}

    You've found either a bug in Loveline or a missing error message.
    Please report the following stack trace to
    https://gitlab.com/ezrast/loveline/issues or ezrastevens@cloudandtree.com.


    EOS
  err.inspect_with_backtrace(STDERR)
  exit 1
end
