require "./cmd_config_dump"
require "./cmd_config_sample"
require "./cmd_config_sample_default"
require "./cmd_config_sample_minimal"

class Loveline
  class CmdConfig
    include Blarg::Command
    @@blarg_description = "Subcommands for working with configuration"
    @@blarg_subname = "config"

    @[Subcommand]
    getter subcmd : (
      CmdConfigDump |
      CmdConfigSample |
      CmdConfigSampleDefault |
      CmdConfigSampleMinimal)
  end

  def process(cmd : CmdConfig)
    process(cmd.subcmd)
  end
end
