require "yaml"

require "./config"

class Loveline
  class CmdConfigDump
    include Blarg::Command
    @@blarg_description = "Prints the current working configuration"
    @@blarg_subname = "dump"
  end

  def process(cmd : CmdConfigDump)
    @config.unbake.to_yaml(@output)
  end
end
