require "yaml"

require "./config"

class Loveline
  class CmdConfigSample
    include Blarg::Command
    @@blarg_description = "Prints a sample config file with all options enabled"
    @@blarg_subname = "sample"
  end

  def process(cmd : CmdConfigSample)
    Config::Raw::SAMPLE.to_yaml(@output)
  end
end
