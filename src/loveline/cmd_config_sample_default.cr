require "yaml"

require "./config"

class Loveline
  class CmdConfigSampleDefault
    include Blarg::Command
    @@blarg_description = "Prints a sample config containing resolved defaults for all options"
    @@blarg_subname = "sample-default"
  end

  def process(cmd : CmdConfigSampleDefault)
    Config.new(Config::Raw::SAMPLE_DEFAULT, input_tty: @input.tty?, output_tty: @output.tty?).unbake.to_yaml(@output)
  end
end
