require "yaml"

require "./config"

class Loveline
  class CmdConfigSampleMinimal
    include Blarg::Command
    @@blarg_description = "Prints a sample config file with all options disabled"
    @@blarg_subname = "sample-minimal"
  end

  def process(cmd : CmdConfigSampleMinimal)
    Config::Raw::SAMPLE_MINIMAL.to_yaml(@output)
  end
end
