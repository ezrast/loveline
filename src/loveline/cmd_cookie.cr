require "./laminatable_command"
require "./cmd_cookie_create"
require "./cmd_cookie_delete"
require "./cmd_cookie_end_session"
require "./cmd_cookie_list"

class Loveline
  class CmdCookie
    include Blarg::Command
    @@blarg_description = "Subcommands for working with cookies"
    @@blarg_subname = "cookie"

    @[Subcommand]
    getter subcmd : (
      CmdCookieCreate |
      CmdCookieDelete |
      CmdCookieEndSession |
      CmdCookieList)
  end

  def process(cmd : CmdCookie)
    process(cmd.subcmd)
  end
end
