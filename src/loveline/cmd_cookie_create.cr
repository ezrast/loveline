require "./laminatable_command"

class Loveline
  class CmdCookieCreate
    include Blarg::Command
    @@blarg_description = "Creates a cookie"
    @@blarg_subname = "create"

    @[Positional(description: "The host and (optionally) path for your cookie, such as example.org/get/123")]
    getter resource : String

    @[Positional(description: "The name of the cookie to create")]
    getter name : String

    @[Positional(description: "The value the cookie will hold")]
    getter value : String

    # TODO we have no scheme for parsing time from the command line.
    # @[Flag(short: 'e', description: "Expire the cookie at the given time")]
    # getter expiry : Time?

    @[Flag(short: 's', description: "Only send the cookie via HTTPS connections")]
    getter secure : Bool

    # http_only doesn't mean anything to loveline
    # @[Flag(short: 'a', description: "Hide the cookie from non-HTTP APIs")]
    # getter http_only : Bool

    @[Flag(short: 'o', description: "Only send the cookie to exact host matches")]
    getter host_only : Bool

    @[Flag(short: 'f', description: "Overwrite any existing cookie with the same domain/path/name")]
    getter force : Bool
  end

  def process(cmd : CmdCookieCreate)
    if slash_idx = cmd.resource.index('/')
      domain = cmd.resource[0...slash_idx]
      path = cmd.resource[slash_idx..-1]
    else
      domain = cmd.resource
      path = "/"
    end
    now = Time.utc
    cookie = Cookie.new(
      domain: domain,
      path: path,
      name: cmd.name,
      value: cmd.value,
      creation_time: now,
      last_access_time: now,
      expiry: nil,
      secure: cmd.secure,
      http_only: false,
      host_only: cmd.host_only
    )

    if (cookie_jar = @config.cookie_jar).is_a? WriteableCookieJar
      cookie_jar.save(cookie, cmd.force)
    else
      raise "Cookie jar is not writeable" # TODO real exception
    end
  end
end
