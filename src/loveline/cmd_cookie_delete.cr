require "./cookie_selector_command"

class Loveline
  class CmdCookieDelete
    include CookieSelectorCommand
    @@blarg_description = "Deletes stored cookies"
    @@blarg_subname = "delete"
  end

  def process(cmd : CmdCookieDelete)
    cookie_jar = @config.cookie_jar
    unless cookie_jar.is_a? WriteableCookieJar
      @output << "Cookie jar is not writeable\n".colorize.red
      return
    end

    cookies = cmd.get_cookies(@config.cookie_jar)

    if cookies.empty?
      @output << "No cookies found\n".colorize.red
    else
      cookies.each do |cookie|
        ll_puts "Deleting #{cookie.domain.colorize.cyan}#{cookie.path.colorize.light_cyan} #{cookie.name}..."
        cookie_jar.delete(cookie)
      end
    end
  end
end
