class Loveline
  class CmdCookieEndSession
    include Blarg::Command
    @@blarg_description = "Deletes non-persistent cookies"
    @@blarg_subname = "end-session"
  end

  def process(cmd : CmdCookieEndSession)
    cookie_jar = @config.cookie_jar
    unless cookie_jar.is_a? WriteableCookieJar
      @output << "Cookie jar is not writeable\n".colorize.red
      return
    end

    cookies = cookie_jar.search(
      "",
      "/",
      true,
      Time.utc,
      domain_matcher: Cookie::DomainMatcher::Subdomains,
      path_matcher: Cookie::PathMatcher::Subpaths,
    ).reject{ |cookie| cookie.persistent? }

    if cookies.empty?
      @output << "No non-persistent cookies found\n".colorize.red
      return
    end

    cookies.each do |cookie|
      ll_puts "Deleting #{cookie.domain.colorize.cyan}#{cookie.path.colorize.light_cyan} #{cookie.name}..."
      cookie_jar.delete(cookie)
    end
  end
end
