require "./laminatable_command"

class Loveline
  class CmdCookieList
    include CookieSelectorCommand
    @@blarg_description = "Lists stored cookies"
    @@blarg_subname = "list"

    @[Flag(short: 'c', description: "Shows cookie creation time")]
    getter show_creation_time : Bool = false
  end

  def process(cmd : CmdCookieList)
    if @config.cookie_jar.is_a? NullCookieJar
      @output << "No cookie jar configured\n".colorize.red
      return
    end

    cookies = cmd.get_cookies(@config.cookie_jar)

    if cookies.empty?
      @output << "No cookies found\n".colorize.red
    else
      cookies.each do |cookie|
        pretty_print_cookie(
          cookie,
          show_creation_time: cmd.show_creation_time
        )
      end
    end
  end
end
