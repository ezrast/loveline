require "./laminatable_command"

class Loveline
  class CmdCreate
    include LaminatableCommand
    @@blarg_description = "Laminates a template and saves it to disk"
    @@blarg_subname = "create"

    @[Flag(short: 'n', description: "Your new template's name")]
    getter name : String

    @[Flag(short: 'f', description: "Overwrite any existing homonymous template")]
    getter force : Bool
  end

  def process(cmd : CmdCreate)
    template_dirs = @config.all_template_paths.compact_map(&.expand_if_exists)
    tpl = cmd.laminate(template_dirs)
    save_path = @config.template_dir.expand_and_create_with_default_permissions
    tpl.flatten(cmd.name).save(save_path, cmd.force)
  end
end
