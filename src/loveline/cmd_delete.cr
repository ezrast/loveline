require "./pretty_print"

class Loveline
  class CmdDelete
    include Blarg::Command
    @@blarg_description = "Deletes templates"
    @@blarg_subname = "delete"

    @[Positional(description: "The templates to delete")]
    getter names : Array(String)
  end

  def process(cmd : CmdDelete)
    template_dir = @config.template_dir.expand_if_exists
    unless template_dir
      ll_puts "Template directory #{template_dir} does not exist"
      return
    end

    cmd.names.each do |name|
      if Template.delete(template_dir, name)
        ll_puts "Deleted `#{name}` from #{template_dir}"
      else
        ll_puts "No template `#{name}` found in #{template_dir}"
      end
    end
  end
end
