require "blarg"
require "http"

require "./cookie"
require "./interpolator"
require "./laminatable_command"
require "./pretty_print"
require "./template"

class Loveline
  class CmdExec
    include LaminatableCommand
    @@blarg_description = "Laminates a template and executes it as an HTTP request"
    @@blarg_subname = "exec"

    @[Flag(short: 'd', description: "Print request details but do not make the request")]
    getter dry : Bool

    @[Flag(short: 'c', description: "Override content type handler for this request (`none` to disable)")]
    getter content_handler : String?
  end

  def process(cmd : CmdExec)
    template_libraries = @config.all_template_paths.compact_map(&.expand_if_exists)
    cookie_jar = @config.cookie_jar

    template = cmd.laminate(template_libraries)
    request = Request.new(template, @asker, cookie_jar, @config.insecure_tls, user_agent: USER_AGENT)

    if cmd.dry
      pretty_print_request(request)
    else
      resp = request.exec

      if cookie_jar.is_a? WriteableCookieJar
        request_host = request.headers.get?("Host").try(&.first) || request.url.host.not_nil!
        Cookie.from_headers(resp.headers, request_host, request.url.path, request.url.scheme == "https").each do |cookie|
          cookie.update_creation_time!(cookie_jar)
          cookie_jar.save(cookie, true)
        end
      end

      pretty_print_response(resp)
      handle_response_body(resp, cmd.content_handler)
    end
  end

  def handle_response_body(resp, content_handler_override)
    mime_type = resp.headers["Content-Type"]?.try do |content_type|
      content_type.try(&.split(";").first?)
    end

    case content_handler_override
    when "none"
      outputter = nil
    when nil
      outputter = @config.content_type_handlers[mime_type]? || @config.content_type_handlers["*"]?
    else
      outputter = content_handler_override
    end

    if outputter.is_a? String
      outputter = outputter.empty? ? nil : outputter.split
    end

    if outputter.nil?
      unless resp.body.empty?
        pretty_print_body(resp.body)
      end
      return
    end

    bin_path = outputter.shift
    args = outputter

    if bin_path.starts_with? "|"
      pipe = true
      bin_path = bin_path[1..-1]
    else
      pipe = false
    end

    begin
      tempfile = nil

      subbed_args = args.map do |arg|
        arg.gsub(/(%%s|%s)/) do |str|
          if str == "%%s"
            "%s"
          else
            tempfile ||= File.tempfile("loveline", ".#{mime_type.to_s.split("/").last}")
            tempfile.path
          end
        end
      end

      if tempfile
        tempfile.print(resp.body)
        tempfile.close
      end

      if pipe
        @output << "Piping body to: ".colorize.red << bin_path
        args.each{ |arg| @output << ' ' << arg }
        @output << "\n"
        Process.run(
          bin_path,
          subbed_args,
          input: Process::Redirect::Pipe,
          output: @output,
          error: @output # TODO differentiate stdout/stderr?
        ) do |process|
          process.input.print(resp.body)
          process.input.close
        end
      else
        @output << "Opening body with: ".colorize.red << bin_path
        args.each{ |arg| @output << ' ' << arg }
        @output << "\n"
        Process.run(
          bin_path,
          subbed_args,
          input: @input,
          output: @output,
          error: @output # TODO differentiate stdout/stderr?
        )
      end
    ensure
      tempfile.try(&.delete)
    end
  end
end
