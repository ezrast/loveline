require "blarg"
require "ecr"

require "./interpolator"
require "./laminatable_command"
require "./request"
require "./template"

class Loveline
  class CmdExport
    include Blarg::Command
    @@blarg_description = "Laminates a template and shows how to execute it using a foreign toolset"
    @@blarg_subname = "export"

    @[Subcommand]
    getter exporter : (
      CmdExportCrystal |
      CmdExportCurl |
      CmdExportJinja |
      CmdExportPythonRequests)
  end

  def process(cmd : CmdExport)
    template_libraries = @config.all_template_paths.compact_map(&.expand_if_exists)
    template = cmd.exporter.laminate(template_libraries)

    cmd.exporter.export(@output, Request.new(template, @asker, @config.cookie_jar, @config.insecure_tls))
  end

  class CmdExportCrystal
    include LaminatableCommand
    @@blarg_description = "Laminates a template and shows how to execute it using Crystal"
    @@blarg_subname = "crystal"

    def export(io, request)
      request.render_crystal(io)
    end
  end

  class CmdExportCurl
    include LaminatableCommand
    @@blarg_description = "Laminates a template and shows how to execute it using Curl"
    @@blarg_subname = "curl"

    def export(io, request)
      request.render_curl(io)
    end
  end

  class CmdExportJinja
    include LaminatableCommand
    @@blarg_description = "Laminates a template and renders a Jinja2 document using the request data"
    @@blarg_subname = "jinja"

    @[Positional(description: "The Jinja2 template file to render")]
    @template_file : String

    def export(io, request)
      request.render_jinja(io, @template_file)
    end
  end

  class CmdExportPythonRequests
    include LaminatableCommand
    @@blarg_description = "Laminates a template and shows how to execute it using Requests"
    @@blarg_subname = "python-requests"

    def export(io, request)
      request.render_python_requests(io)
    end
  end
end
