require "./config"

class Loveline
  class CmdList
    include Blarg::Command
    @@blarg_description = "Lists all saved templates"
    @@blarg_subname = "list"
  end

  def process(cmd : CmdList)
    @config.all_template_paths.each do |path|
      handle_path(path)
    end
  end

  def handle_path(path)
    ll_puts "In #{path.value.colorize.yellow}:"
    if expanded = path.expand_if_exists
      filenames = Dir.children(expanded)
      if filenames.empty?
        ll_puts "  No templates found"
      else
        filenames.sort.each{ |fn| ll_puts "- #{fn}" }
      end
    else
      ll_puts "  Directory does not exist"
    end
    ll_puts ""
  end
end
