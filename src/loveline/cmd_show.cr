require "./pretty_print"

class Loveline
  class CmdShow
    include LaminatableCommand
    @@blarg_description = "Laminates a template and pretty-prints it"
    @@blarg_subname = "show"
  end

  def process(cmd : CmdShow)
    template_libraries = @config.all_template_paths.compact_map(&.expand_if_exists)
    template = cmd.laminate(template_libraries)
    pretty_print_template(template)
  end
end
