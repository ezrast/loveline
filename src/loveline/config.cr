require "./config/enums"
require "./config/path"
require "./config/raw"
require "./cookie_jar"

class Loveline
  struct Config
    getter template_libraries : Array(Path)
    getter template_dir : Path
    getter cookie_jar : NullCookieJar | DirectoryCookieJar
    getter content_type_handlers : Hash(String, String | Array(String))
    getter color : Bool
    getter truncate_body_after : Int32
    getter interactive : Bool
    getter insecure_tls : Bool

    def initialize(raw_config : Config::Raw, *, input_tty : Bool, output_tty : Bool)
      @template_libraries = raw_config.resolve_template_libraries
      @template_dir = raw_config.resolve_template_dir
      @cookie_jar = raw_config.resolve_cookie_jar
      @content_type_handlers = raw_config.resolve_content_type_handlers
      @color = raw_config.color.to_b{ output_tty }
      @truncate_body_after = raw_config.resolve_truncate_body_after
      @interactive = raw_config.interactive.to_b{ input_tty && output_tty }
      @insecure_tls = raw_config.insecure_tls.to_b{ false }
    end

    def all_template_paths
      ret = Array(Path).new(template_libraries.size + 1)
      ret << @template_dir
      return ret.concat @template_libraries
    end

    def unbake
      Raw.new(
        template_libraries: @template_libraries,
        template_dir: @template_dir,
        cookie_jar: case cj = @cookie_jar
          in NullCookieJar then OptionalEnum::None
          in DirectoryCookieJar then Path.new(cj.dir)
          end,
        content_type_handlers: @content_type_handlers,
        color: Config::Autobool.new(@color),
        truncate_body_after: @truncate_body_after,
        interactive: Config::Autobool.new(@interactive),
        insecure_tls: Config::Autobool.new(@insecure_tls),
      )
    end
  end
end
