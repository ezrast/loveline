require "yaml"

struct Enum
  def to_yaml(yaml : YAML::Nodes::Builder)
    yaml.scalar(to_s.downcase)
  end

  def self.new(ctx : YAML::ParseContext, node : YAML::Nodes::Node)
    unless node.is_a?(YAML::Nodes::Scalar)
      node.raise "Expected scalar, not #{node.class}"
    end
    return parse(node.value)
  rescue err : ArgumentError
    node.raise(err.message)
  end
end

class Loveline
  struct Config
    enum Autobool
      False
      True
      Default

      def to_b : Bool
        case self
        when False then false
        when True then true
        else yield
        end
      end

      def self.new(bool : Bool)
        bool ? True : False
      end
    end

    enum DefaultEnum
      Default
    end

    enum OptionalEnum
      None
      Default
    end
  end
end
