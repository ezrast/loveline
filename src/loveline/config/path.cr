class Loveline
  struct Config
    struct Path
      getter value : String

      def initialize(ctx : YAML::ParseContext, node : YAML::Nodes::Node)
        unless node.is_a?(YAML::Nodes::Scalar)
          node.raise "Expected scalar, not #{node.class}"
        end

        case value = node.value
        when .starts_with?("/"), .starts_with?("./"), .starts_with?("~/"), ".", "~"
          @value = value
        else
          node.raise "Relative paths must have . or ~ as their first component"
        end
      end

      def initialize(@value)
      end

      def to_yaml(*args, **kwargs)
        @value.to_yaml(*args, **kwargs)
      end

      def expand_if_exists : String?
        if File.exists?(ex = expand_without_creating)
          ex
        end
      end

      def expand_and_create_with_default_permissions : String
        unless File.exists?(ex = expand_without_creating)
          Dir.mkdir_p(ex)
        end
        return ex
      end

      def expand_without_creating : String
        if @value.starts_with?("~")
          unless home_dir = ENV["HOME"]?
            raise ConfigError.new("Config error expanding #{@value}: $HOME is unset")
          end
          return File.expand_path(home_dir + @value.lchop)
        end
        return File.expand_path(@value)
      end
    end
  end
end
