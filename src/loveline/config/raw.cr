require "yaml"

require "./enums"

class Loveline
  struct Config
    struct Raw
      include YAML::Serializable
      include YAML::Serializable::Strict

      property template_libraries : Array(Path) | DefaultEnum = ::Loveline::Config::DefaultEnum::Default
      property template_dir : Path | DefaultEnum = ::Loveline::Config::DefaultEnum::Default
      property cookie_jar : Path | OptionalEnum = ::Loveline::Config::OptionalEnum::Default
      property content_type_handlers : Hash(String, String | Array(String)) | DefaultEnum = ::Loveline::Config::DefaultEnum::Default
      property color : Autobool = ::Loveline::Config::Autobool::Default
      property truncate_body_after : Int32 | DefaultEnum = ::Loveline::Config::DefaultEnum::Default
      property interactive : Autobool = ::Loveline::Config::Autobool::Default
      property insecure_tls : Autobool = ::Loveline::Config::Autobool::Default

      def initialize(*,
        @template_libraries,
        @template_dir,
        @cookie_jar,
        @content_type_handlers,
        @color,
        @truncate_body_after,
        @interactive,
        @insecure_tls)
      end

      def resolve_template_libraries
        case dirs = @template_libraries
        when Array(Path)
          dirs
        when DefaultEnum
          if (xdg_dirs = ENV["XDG_DATA_DIRS"]?) && !xdg_dirs.empty?
            raw_paths = xdg_dirs.split(':').reject(&.empty?)
            raw_paths.map do |path|
              Path.new("#{path}/loveline/templates")
            end
          else
            [
              Path.new("/usr/local/share/loveline/templates"),
              Path.new("/usr/share/loveline/templates"),
            ]
          end
        else # unreachable
          raise ConfigError.new("Config error parsing template_libraries: must be `default` or an array of paths")
        end
      end

      def resolve_template_dir
        case td = @template_dir
        when Path
          td
        when DefaultEnum
          xdg_dir = ENV["XDG_DATA_HOME"]?
          return Path.new("#{xdg_dir}/loveline/templates") if xdg_dir && !xdg_dir.empty?

          home_dir = ENV["HOME"]?
          return Path.new("#{home_dir}/.local/share/loveline/templates") if home_dir && !home_dir.empty?
          raise ConfigError.new("Config error using default template_dir: no path given and $HOME unset")
        else # unreachable
          raise ConfigError.new("Config error parsing template_dir: must be `default` or a path")
        end
      end

      def resolve_cookie_jar : CookieJar
        case cj = @cookie_jar
        when Path
          DirectoryCookieJar.new cj
        when OptionalEnum::None
          NullCookieJar.new
        when OptionalEnum::Default
          xdg_dir = ENV["XDG_DATA_HOME"]?
          return DirectoryCookieJar.new_no_expansion("#{xdg_dir}/loveline/cookies") if xdg_dir && !xdg_dir.empty?

          home_dir = ENV["HOME"]?
          return DirectoryCookieJar.new_no_expansion("#{home_dir}/.local/share/loveline/cookies") if home_dir && !home_dir.empty?
          return NullCookieJar.new
        else # unreachable
          raise ConfigError.new("Config error parsing cookie_jar: must be `default` or a path")
        end
      end

      def resolve_content_type_handlers
        case cth = @content_type_handlers
        when Hash(String, String | Array(String))
          cth
        when DefaultEnum
          Hash(String, String | Array(String)).new
        else # unreachable
          raise ConfigError.new("Config error parsing content_type_handlers: must be `default` or a mapping")
        end
      end

      def resolve_truncate_body_after
        case tba = @truncate_body_after
        when Int32
          tba
        when DefaultEnum
          1024
        else # unreachable
          raise ConfigError.new("Config error parsing truncate_body_after: must be `default` or an integer")
        end
      end

      SAMPLE = Raw.new(
        template_libraries: [
          Path.new("/var/lib/loveline/templates"),
          Path.new("./.loveline/templates"),
        ],
        template_dir: Path.new("~/.config/loveline/templates"),
        cookie_jar: Path.new("~/.local/share/loveline/cookies"),
        content_type_handlers: {
          "application/json" => "|/usr/bin/jq",
          "text/html" => "/usr/bin/links %s"
        } of String => String | Array(String),
        color: Loveline::Config::Autobool::True,
        truncate_body_after: 4096,
        interactive: Loveline::Config::Autobool::True,
        insecure_tls: Loveline::Config::Autobool::True,
      )

      SAMPLE_MINIMAL = Raw.new(
        template_libraries: [] of Path,
        template_dir: Path.new("./.loveline/templates"),
        cookie_jar: Loveline::Config::OptionalEnum::None,
        content_type_handlers: {} of String => String | Array(String),
        color: Loveline::Config::Autobool::False,
        truncate_body_after: 0,
        interactive: Loveline::Config::Autobool::False,
        insecure_tls: Loveline::Config::Autobool::False,
      )

      SAMPLE_DEFAULT = Raw.from_yaml("")

      def merge!(yaml : String)
        hash = Hash(String, YAML::Any).from_yaml(yaml)
        {% for var in @type.instance_vars %}
          if val = hash.delete( {{ var.stringify }} )
            @{{ var.id }} = ({{ var.type }}).from_yaml(val.to_yaml)
          end
        {% end %}

        unless hash.empty?
          raise ConfigError.new("Unexpected config keys: #{hash.keys.join(", ")}")
        end
      end

      def self.from_yaml(*args)
        super(*args)
      rescue err : YAML::ParseException
        raise ConfigError.new("Error parsing configuration yaml: #{err}")
      end
    end
  end
end
