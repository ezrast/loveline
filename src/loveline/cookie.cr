require "http"
require "yaml"

require "./data_version"

class Loveline
  class Cookie
    include YAML::Serializable
    include YAML::Serializable::Strict

    DataVersion[ck_v1]

    getter domain : String
    getter path : String
    getter name : String
    getter value : String

    getter creation_time : Time
    getter last_access_time : Time
    getter expiry : Time?

    getter secure : Bool
    getter http_only : Bool
    getter host_only : Bool

    def_equals(domain, path, name, value,
      creation_time, last_access_time, expiry,
      secure, http_only, host_only
    )

    def initialize(*,
      domain,
      path,
      name,
      @value,
      @creation_time,
      @last_access_time,
      @expiry,
      @secure,
      @http_only,
      @host_only
    )
      raise BadCookieDomain.new(domain) unless domain =~ /\A[a-zA-Z0-9-\.]+\z/ || self.class.ip?(domain)
      @domain = domain
      @path = (path.starts_with?("/") ? path : "/")
      raise EmptyCookieName.new if name.empty?
      @name = name
    end

    def expired?(current_time : Time) : Bool
      return false unless expiry = @expiry
      return current_time > expiry
    end

    def update_creation_time!(cookie_jar)
      cookie_jar.load_exact?(@domain, @path, @name).try do |old_cookie|
        @creation_time = {@creation_time, old_cookie.creation_time}.min
      end
    end

    def persistent?
      !expiry.nil?
    end

    def self.from_headers(headers : HTTP::Headers, request_host : String, request_path : String, request_secure : Bool) : Array(Cookie)
      return [] of Cookie unless set_cookie_headers = headers.get?("Set-Cookie")
      return set_cookie_headers.compact_map do |str|
        parse_set_cookie_string(str, request_host, request_path, request_secure)
      end
    end

    # The odd variable names in this method are taken directly from the
    # algorithm specified in RFC6265 section 5.2:
    # https://tools.ietf.org/html/rfc6265#section-5.2
    def self.parse_set_cookie_string(set_cookie_string : String, request_host : String, request_path : String, request_secure : Bool) : Cookie?
      now = Time.utc
      name_value_pair, _, unparsed_attributes = set_cookie_string.partition(";")
      name, eq, value = name_value_pair.partition("=")
      return if eq.empty?
      cookie_name = name.strip(" \t")
      return if cookie_name.empty?
      cookie_value = value.strip(" \t")

      default_path = (chopped_path = request_path.rchop('/')).starts_with?('/') ? chopped_path : "/"

      expires : Time? = nil
      max_age : Time? = nil
      domain : String = ""
      host_only : Bool = false
      path : String = default_path
      secure_only : Bool = false
      http_only : Bool = false

      unparsed_attributes.split(";").each do |cookie_av|
        attribute_name, _, attribute_value = cookie_av.partition("=")
        attribute_name = attribute_name.strip
        attribute_value = attribute_value.strip
        case attribute_name.downcase
        when "expires"
          if date = parse_date(attribute_value)
            expires = date
          end
        when "max-age"
          delta_seconds = attribute_value.to_i64?
          if delta_seconds
            if delta_seconds > 0
              max_age = now + delta_seconds.seconds
            else
              # spec says to use the earliest representable time but
              # time zones complicate that; do the next best thing.
              max_age = Time::UNIX_EPOCH
            end
          end
        when "domain"
          unless attribute_value.empty?
            domain = attribute_value.lchop('.').downcase
          end
        when "path"
          path = attribute_value.starts_with?('/') ? attribute_value : default_path
        when "secure"
          # Not to spec, but modern browsers don't let insecure sites
          # set secure cookies.
          return nil unless request_secure
          secure_only = true
        when "httponly"
          http_only = true
        end
      end

      if domain.empty?
        host_only = true
        domain = request_host
      else
        return nil unless Cookie.domain_match?(
          request_host: request_host,
          cookie_domain: domain
        )
      end

      return Cookie.new(
        name: cookie_name,
        value: cookie_value,
        creation_time: now,
        last_access_time: now,
        expiry: max_age || expires,
        domain: domain,
        host_only: host_only,
        path: path,
        secure: secure_only,
        http_only: http_only
      )
    end

    DELIMITER = /[\x09\x20-\x2F\x3B-\x40\x5B-\x60\x7B-\x7E]+/
    NON_DIGIT = /\z|\D/
    TIME = /\A(?<hour>\d\d?):(?<minute>\d\d?):(?<second>\d\d?)#{NON_DIGIT}/
    DAY_OF_MONTH = /\A(?<day>\d\d?)#{NON_DIGIT}/
    YEAR = /\A(?<year>\d{2,4})#{NON_DIGIT}/

    enum Months
      Jan = 1
      Feb
      Mar
      Apr
      May
      Jun
      Jul
      Aug
      Sep
      Oct
      Nov
      Dec
    end

    def self.parse_date(str : String) : Time?
      tokens = str.split(DELIMITER).reject(&.empty?)

      time = day_of_month = month = year = nil
      tokens.each do |token|
        if !time && (match = TIME.match token)
          time = {hour: match["hour"].to_i, minute: match["minute"].to_i, second: match["second"].to_i}
        elsif !day_of_month && (match = DAY_OF_MONTH.match token)
          day_of_month = match["day"].to_i
        elsif !month && (month_enum = Months.parse?(token[0..2]))
          month = month_enum.to_i
        elsif !year && (match = YEAR.match token)
          year = match["year"].to_i
        end
      end

      return nil unless time && day_of_month && month && year

      year += 1900 if (70..99).includes? year
      year += 2000 if (0..69).includes? year
      return nil if year < 1601

      # These checks are in the spec but redundant with Time.utc
      # return nil unless (1..31).includes? day_of_month
      # return nil if time[:hour] > 23
      # return nil if time[:minute] > 59
      # return nil if time[:second] > 59
      begin
        return Time.utc(year, month, day_of_month, time[:hour], time[:minute], time[:second])
      rescue ArgumentError
        return nil
      end
    end

    def self.domain_match?(*, request_host : String, cookie_domain : String) : Bool
      return true if request_host == cookie_domain
      return false if ip?(request_host)
      cookie_domain = "." + cookie_domain unless cookie_domain.starts_with?(".")
      return request_host.ends_with?(cookie_domain)
    end

    def self.path_match?(*, request_path : String, cookie_path : String) : Bool
      return true if request_path == cookie_path
      cookie_path += "/" unless cookie_path.ends_with?("/")
      return request_path.starts_with?(cookie_path)
    end

    protected def self.ip?(host : String) : Bool
      in6_addr = uninitialized LibC::In6Addr
      return true if LibC.inet_pton(LibC::AF_INET6, host, pointerof(in6_addr)) == 1

      in_addr = uninitialized LibC::InAddr
      return LibC.inet_pton(LibC::AF_INET, host, pointerof(in_addr)) == 1
    end

    # This isn't a rigorous validity check. I mostly just want to stop
    # weird things happening if somebody makes a request to "..".
    # https://www.ietf.org/rfc/rfc952.txt
    # https://www.ietf.org/rfc/rfc1123.txt
    def self.valid_hostname?(str : String) : Bool
      labels = str.split '.'
      return false if labels.empty?
      return labels.all?{ |label| label =~ /\A[a-zA-Z0-9]([-a-zA-Z0-9]*[a-zA-Z0-9])?\z/ }
    end

    enum DomainMatcher
      Exact
      Request
      Subdomains
    end

    enum PathMatcher
      Exact
      Request
      Subpaths
    end
  end
end
