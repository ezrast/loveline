require "digest"
require "uri"

require "./cookie"

class Loveline
  module CookieJar
    abstract def load_exact?(domain : String, path : String, name : String)
    abstract def search(
      request_host : String,
      request_path : String,
      request_secure : Bool,
      now : Time,
      *,
      name : String? = nil,
      domain_matcher : Cookie::DomainMatcher,
      path_matcher : Cookie::PathMatcher,
    )

    def get_all
      search("", "/", true, Time.utc,
        domain_matcher: Loveline::Cookie::DomainMatcher::Subdomains,
        path_matcher: Loveline::Cookie::PathMatcher::Subpaths
      )
    end
  end

  module WriteableCookieJar
    abstract def save(cookie : Cookie, force : Bool)
    abstract def delete(cookie : Cookie)
  end

  class NullCookieJar
    include CookieJar

    def load_exact?(domain : String, path : String, name : String) : Cookie?
      nil
    end

    def search(
      request_host : String,
      request_path : String,
      request_secure : Bool,
      now : Time,
      *,
      name : String? = nil,
      domain_matcher : Cookie::DomainMatcher,
      path_matcher : Cookie::PathMatcher,
    ) : Array(Cookie)
      [] of Cookie
    end
  end

  class DirectoryCookieJar
    include CookieJar
    include WriteableCookieJar

    getter dir : String

    def initialize(dir : Loveline::Config::Path)
      @dir = dir.expand_without_creating
    end

    private def initialize(@dir : String)
    end

    def self.new_no_expansion(dir : String)
      new(dir)
    end

    def load_exact?(domain : String, path : String, name : String) : Cookie?
      encoded_path = DirectoryCookieJar.filename_encode(path)
      encoded_name = DirectoryCookieJar.filename_encode(name)
      file_path = "#{@dir}/#{domain}/#{encoded_path}/#{encoded_name}"

      return nil unless File.exists? file_path
      return File.open(file_path, "rb"){ |ff| Cookie.from_yaml(ff) }
    end

    private def conditional_load_or_expire(cookie_file_path : String, request_host : String, now : Time, request_secure : Bool, domain_matcher : Cookie::DomainMatcher) : Cookie?
      cookie = File.open(cookie_file_path){ |ff| Cookie.from_yaml(ff) }
      if cookie.expired?(now)
        File.delete(cookie_file_path)
        return nil
      end
      return nil if cookie.secure && !request_secure

      check_host_only = case domain_matcher
      in .request? then true
      in .exact?, .subdomains? then false
      end

      return nil if check_host_only && cookie.host_only && cookie.domain != request_host
      return cookie
    end

    def search(
      request_host : String,
      request_path : String,
      request_secure : Bool,
      now : Time,
      *,
      name : String? = nil,
      domain_matcher : Cookie::DomainMatcher,
      path_matcher : Cookie::PathMatcher,
    ) : Array(Cookie)
      request_path = "/" unless request_path.starts_with? '/'

      ret = [] of Cookie
      return ret unless Dir.exists? @dir

      each_domain(domain_matcher, request_host) do |domain_dir|
        each_path(path_matcher, domain_dir, request_path) do |path_dir|
          double_check_path = case path_dir
          in String then ->(cookie : Cookie){ true }
          in PartialString
            path_dir = path_dir.value
            ->(cookie : Cookie){
              Cookie.path_match?(
                # inverted intentionally
                cookie_path: request_path,
                request_path: cookie.path,
              )
            }
          end

          if name
            cookie_file_path = "#{path_dir}/#{name}"
            if File.exists?(cookie_file_path)
              if cookie = conditional_load_or_expire(cookie_file_path, request_host, now, request_secure, domain_matcher)
                ret << cookie if double_check_path.call(cookie)
              end
            end
          else
            Dir.each_child(path_dir) do |filename|
              cookie_file_path = "#{path_dir}/#{filename}"
              if cookie = conditional_load_or_expire(cookie_file_path, request_host, now, request_secure, domain_matcher)
                ret << cookie if double_check_path.call(cookie)
              end
            end
          end
          Dir.delete path_dir if Dir.empty? path_dir
        end
        Dir.delete domain_dir if Dir.empty? domain_dir
      end

      return ret
    end

    def each_domain(matcher : Cookie::DomainMatcher, request_host : String)
      case matcher
      in .exact?
        return unless Cookie.valid_hostname? request_host
        if Dir.exists?(dir = "#{@dir}/#{request_host}")
          yield dir
        end

      in .request?
        return unless Cookie.valid_hostname? request_host
        if Cookie.ip?(request_host)
          if Dir.exists?(dir = "#{@dir}/#{request_host}")
            yield dir
          end
          return
        end

        idx = 0
        while true
          superdomain_dir = "#{@dir}/#{request_host[idx..-1]}"
          yield superdomain_dir if Dir.exists?(superdomain_dir)
          break unless next_dot = request_host.index('.', idx)
          idx = next_dot + 1
        end

      in .subdomains?
        Dir.new(@dir).each_child do |child|
          yield "#{@dir}/#{child}" if request_host.empty? || Cookie.domain_match?(
            # inverted intentionally
            request_host: child,
            cookie_domain: request_host,
          )
        end
      end
    end

    def each_path(matcher : Cookie::PathMatcher, domain_dir : String, request_path : String)
      case matcher
      in .exact?
        if Dir.exists?(dir = "#{domain_dir}/#{DirectoryCookieJar.filename_encode(request_path)}")
          yield dir
        end

      in .request?
        superpath = request_path
        while true
          superpath_dir = "#{domain_dir}/#{DirectoryCookieJar.filename_encode(superpath)}"
          yield superpath_dir if Dir.exists? superpath_dir

          if superpath.ends_with? '/'
            return if superpath.size == 1
            superpath = superpath[0...-1]
          else
            last_slash_idx = superpath.rindex('/')
            return unless last_slash_idx
            superpath = superpath[0..last_slash_idx]
          end
        end

      in .subpaths?
        # Encoded directory names can't be fully decoded (because
        # long URL paths must be truncated to abide by UNIX directory
        # name length limits) so some matches are only potential/partial
        # matches. It's the calling method's job to deal with this.
        Dir.new(domain_dir).each_child do |child|
          decode_result = DirectoryCookieJar.filename_decode(child)
          case decode_result
          in PartialString
            if request_path.starts_with? decode_result.value
              yield PartialString.new("#{domain_dir}/#{child}")
            end
          in String
            yield "#{domain_dir}/#{child}" if Cookie.path_match?(
              # inverted intentionally
              cookie_path: request_path,
              request_path: decode_result,
            )
          end
        end
      end
    end

    record PartialString,
      value : String

    def save(cookie : Cookie, force : Bool)
      unless Dir.exists? @dir
        parent = File.dirname @dir
        Dir.mkdir_p(parent)
        Dir.mkdir(@dir, 0o700)
      end
      dir_path = "#{@dir}/#{cookie.domain}/#{DirectoryCookieJar.filename_encode(cookie.path)}"
      file_path = "#{dir_path}/#{DirectoryCookieJar.filename_encode(cookie.name)}"

      if File.exists?(file_path) && !force
        raise CookieNameCollision.new(cookie.name, dir_path)
      end

      Dir.mkdir_p(dir_path)
      File.open(file_path, "wb") do |ff|
        cookie.to_yaml(ff)
      end
    end

    def delete(cookie : Cookie) : Nil
      encoded_path = DirectoryCookieJar.filename_encode(cookie.path)
      encoded_name = DirectoryCookieJar.filename_encode(cookie.name)
      file_path = "#{@dir}/#{cookie.domain}/#{encoded_path}/#{encoded_name}"
      File.delete file_path if File.exists? file_path
    end

    ENCODING_SEPARATOR = ":#"
    # Linux can't handle filenames longer than 255 bytes; we need 42
    # for a sha1sum & separator and we'll leave a few left over
    MAX_ENCODED_PATH_LENGTH = 200
    def self.filename_encode(str : String) : String
      String.build do |enc|
        str.each_char do |char|
          encoded_char = case char
          when '/' then "::"
          when ':' then ":."
          else char
          end

          if enc.bytesize + encoded_char.bytesize > MAX_ENCODED_PATH_LENGTH
            enc << ENCODING_SEPARATOR << Digest::SHA1.hexdigest(str)
            break
          end
          enc << encoded_char
        end
      end
    end

    def self.filename_decode(str : String) : String | PartialString
      truncated, divider, sha1 = str.partition(ENCODING_SEPARATOR)
      decoded = truncated.gsub(/:.?/) do |escape|
        case escape[1]?
        when '.' then ":"
        when ':' then "/"
        else
          raise BadFilenameEscape.new(str, escape)
        end
      end

      return divider.empty? ? decoded : PartialString.new(decoded)
    end
  end
end
