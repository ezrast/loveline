require "blarg"

class Loveline
  private module CookieSelectorCommand
    macro included
      include Blarg::Command
    end

    @[Blarg::Annotations::Positional(description: "Selects cookies under the given host and path", order: -9999)]
    getter resource : String?

    @[Blarg::Annotations::Positional(description: "Restricts selection to cookies with the given name", order: -9998)]
    getter name : String?

    @[Blarg::Annotations::Flag(short: 'r', description: "Reverses path and domain matching; selects all if no resource given")]
    getter reverse : Bool = false

    @[Blarg::Annotations::Flag(short: 'd', description: "Restricts domain to exact matches")]
    getter exact_domain : Bool = false

    @[Blarg::Annotations::Flag(short: 'p', description: "Restricts path to exact matches")]
    getter exact_path : Bool = false

    def get_cookies(cookie_jar : CookieJar) : Array(Cookie)
      if rr = resource
        if first_slash_idx = rr.index('/')
          domain = rr[0...first_slash_idx]
          path = rr[first_slash_idx..-1]
        else
          domain = rr
          path = "/"
        end
      else
        domain = ""
        path = "/"
      end

      domain_matcher = if @exact_domain
        Cookie::DomainMatcher::Exact
      elsif @reverse
        Cookie::DomainMatcher::Subdomains
      else
        Cookie::DomainMatcher::Request
      end

      path_matcher = if @exact_path
        Cookie::PathMatcher::Exact
      elsif @reverse
        Cookie::PathMatcher::Subpaths
      else
        Cookie::PathMatcher::Request
      end

      cookies = cookie_jar.search(
        domain,
        path,
        true,
        Time.utc,
        name: @name,
        domain_matcher: domain_matcher,
        path_matcher: path_matcher,
      )
      cookies.select!{ |cookie| cookie.name == name } if name = @name
      cookies.sort_by!{ |cookie| {cookie.domain.split('.').reverse, cookie.path, cookie.name} }
      return cookies
    end
  end
end
