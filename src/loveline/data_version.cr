class Loveline
  module DataVersion
    extend self
    def self.new(ctx : YAML::ParseContext, node : YAML::Nodes::Node)
      self
    end

    def to_yaml(yaml : YAML::Nodes::Builder)
      nil.to_yaml(yaml)
    end

    macro [](version_str)
      @{{ version_str.id }} : Loveline::DataVersion = Loveline::DataVersion
    end
  end
end
