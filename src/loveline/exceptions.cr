require "./interpolator"
require "./template"

class Loveline
  class LovelineException < Exception
  end

  class NoSuchTemplate < LovelineException
    def initialize(name)
      super("Could not find template named #{name}")
    end
  end

  class BadTemplateName < LovelineException
    def initialize(name)
      super("Template names must match #{NAME_REGEX_STR}; got #{name}")
    end
  end

  class TemplateNameCollision < LovelineException
    def initialize(name, dir_path)
      super("Template already exists in #{dir_path}: #{name}; use -f to force")
    end
  end

  class BadVariableName < LovelineException
    def initialize(name)
      super("Variable names must match #{NAME_REGEX_STR}; got #{name}")
    end
  end

  class NoVariableValue < LovelineException
    def initialize(str)
      super("Variables must come in the form NAME=VALUE; got #{str}")
    end
  end

  class BadCookieName < LovelineException
    def initialize(name)
      super("Cookie names may not contain: ^ ( ) < > @ , ; : \\ \" / [ ] ? = { } space tab; got #{name}")
    end
  end

  class NoCookieValue < LovelineException
    def initialize(str)
      super("Cookies must come in the form NAME=VALUE; got #{str}")
    end
  end

  class CookieNameCollision < LovelineException
    def initialize(name, dir_path)
      super("Cookie already exists in #{dir_path}: #{name}; use -f to force")
    end
  end

  class NoHeaderValue < LovelineException
    def initialize(str)
      super("Headers must come in the form NAME=VALUE; got #{str}")
    end
  end

  class RequestNoMethod < LovelineException
    def initialize
      super("Bad request: missing HTTP method (e.g. GET, POST)")
    end
  end

  class RequestNoAddress < LovelineException
    def initialize
      super("Bad request: missing address")
    end
  end

  class RequestUndefinedVariable < LovelineException
    def initialize(var_name)
      super("Bad request: missing variable `#{var_name}`")
    end
  end

  class ConfigError < LovelineException
  end

  class RequestInvalidHostname < LovelineException
    def initialize(uri_str)
      super("Could not parse a hostname from: #{uri_str}")
    end
  end

  class BadCookieDomain < LovelineException
    def initialize(domain_str)
      super("Cookie domain values must be valid hostnames or IP addresses; got #{domain_str}")
    end
  end

  class EmptyCookieName < LovelineException
    def initialize
      super("Cookie names must not be empty")
    end
  end

  class BadFilenameEscape < LovelineException
    def initialize(path, escape)
      super("Invalid escape sequence `#{escape}` in cookie directory at #{path}; use `::` for a slash or `:.` for a colon")
    end
  end

  class BadTemplateEscape < LovelineException
    def initialize(str)
      super("Invalid escape sequence `#{str}` in template. Valid escapes are: %[var], %f[file], %F[file], %%")
    end
  end

  class InterpolationLimitExceeded < LovelineException
    def initialize(str)
      super("Interpolation limit exceeded at depth #{Interpolator::INTERPOLATION_LIMIT} on: #{str}")
    end
  end

  class TemplateError < LovelineException
    def initialize(path, cause)
      super("Error rendering template at #{path}: #{cause}")
    end
  end

  class TemplateFileError < LovelineException
    def initialize(path, cause)
      super(cause.to_s)
    end
  end

  class HttpExecError < LovelineException
    def initialize(cause)
      super("Could not execute HTTP request: #{cause}")
    end
  end

  class TlsError < LovelineException
    def initialize(cause)
      super("Error establishing TLS connection: #{cause}")
    end
  end

  class AddressParseError < LovelineException
    def initialize(uri, cause)
      super("Error parsing address `#{uri}`: #{cause}")
    end
  end
end
