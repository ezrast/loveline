class Loveline
  class Interpolator
    INTERPOLATION_LIMIT = 64
    @variables : Hash(String, String)
    @asked_variables = {} of String => String
    @asker : Proc(String, String?)

    def initialize(@variables, @asker)
    end

    def [](str : String)
      interpolate(str, 0)
    end

    private def interpolate(str : String, depth)
      raise InterpolationLimitExceeded.new(str) if depth > INTERPOLATION_LIMIT
      chars = str.chars
      return String.build do |ret|
        while char = chars.shift?
          if char == '%'
            ret << parse_escape(chars, depth)
          else
            ret << char
          end
        end
      end
    end

    private def parse_escape(chars : Array(Char), depth)
      case char = chars.shift?
      when '%'
        return '%'
      when 'f'
        raise BadTemplateEscape.new("%f") unless chars.shift? == '['
        return File.read(parse_inner(chars, depth))
      when 'F'
        raise BadTemplateEscape.new("%F") unless chars.shift? == '['
        return interpolate(File.read(parse_inner(chars, depth)), depth + 1)
      when 'v'
        raise BadTemplateEscape.new("%v") unless chars.shift? == '['
        return get_var(parse_inner(chars, depth))
      when 'V'
        raise BadTemplateEscape.new("%V") unless chars.shift? == '['
        return interpolate(get_var(parse_inner(chars, depth)), depth + 1)
      when '['
        return interpolate(get_var(parse_inner(chars, depth)), depth + 1)
      else
        raise BadTemplateEscape.new("%#{char}")
      end
    end

    private def parse_inner(chars : Array(Char), depth)
      String.build do |ret|
        until (char = chars.shift?) == ']'
          case char
          when '%'
            ret << parse_escape(chars, depth)
          when nil
            raise BadTemplateEscape.new("%")
          else
            ret << char
          end
        end
      end
    end

    private def get_var(var_name)
      ENV["LOVELINE_VAR_#{var_name}"]? ||
        @variables[var_name]? || (
          @asked_variables[var_name] ||= (
            @asker.call("variable `#{var_name}`") || raise RequestUndefinedVariable.new(var_name)
          )
        )
    end
  end
end
