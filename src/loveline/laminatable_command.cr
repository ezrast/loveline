require "blarg"

class Loveline
  module LaminatableCommand
    macro included
      include Blarg::Command
    end

    @[Blarg::Annotations::Flag(short: 'A', description: "Sets the address in the final template")]
    getter address : String?

    @[Blarg::Annotations::Flag(short: 'B', description: "Sets the body in the final template")]
    getter body : String?

    @[Blarg::Annotations::Flag(short: 'M', description: "Sets the HTTP method in the final template")]
    getter method : String?

    @[Blarg::Annotations::Flag(short: 'H', long: "header", description: "Adds a header to the final template")]
    getter headers : Array(String)

    @[Blarg::Annotations::Flag(short: 'C', long: "cookie", description: "Adds a cookie to the final template")]
    getter cookies : Array(String)

    @[Blarg::Annotations::Flag(short: 'V', long: "var", description: "Sets variables to interpolate into the request")]
    getter variables : Array(String)

    @[Blarg::Annotations::Positional(description: "The templates to laminate", order: 9999)]
    getter names : Array(String)

    def laminate(template_libraries : Array(String))
      templates = Array(ExecutableTemplate).new(names.size + 1)
      names.each do |name|
        addr_index = name.index "://"
        var_index = name.index "="
        if addr_index
          if var_index && (var_index < addr_index)
            @variables << name
          else
            @address = name
          end
        elsif name.includes? "="
          @variables << name
        else
          templates << Template.load(template_libraries, name)
        end
      end
      EphemeralTemplate.from_command?(self).try{ |tpl| templates << tpl }
      return LaminateTemplate.new(templates)
    end
  end
end
