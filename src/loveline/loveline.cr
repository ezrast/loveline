require "blarg"
require "http"

require "./cmd_cookie"
require "./cmd_create"
require "./cmd_delete"
require "./cmd_config"
require "./cmd_exec"
require "./cmd_export"
require "./cmd_list"
require "./cmd_show"
require "./template"


class Loveline
  VERSION = "0.1.0"
  USER_AGENT = "Loveline/#{VERSION} (https://gitlab.com/ezrast/loveline)"

  class CommandLine
    include Blarg::Command

    @[Flag(short: 'c', long: "config", description: "Path to a Loveline config file")]
    getter config_path : String?

    @[Flag(short: 'o', long: "config_option", description: "Overrides config file settings")]
    getter config_options : Array(String)

    @[Subcommand]
    getter subcmd : (
      CmdConfig |
      CmdCookie |
      CmdCreate |
      CmdDelete |
      CmdExec |
      CmdExport |
      CmdList |
      CmdShow)
  end

  @command : CommandLine
  @config : Config
  @input : IO
  @output : IO
  @asker : Proc(String, String?)

  # config, input, and output will always default in real life. They're
  # there to facilitate testing.
  def initialize(args = ARGV, raw_config : Config::Raw? = nil, *, @input = STDIN, @output = STDOUT)
    @command = CommandLine.from_args!(args)
    unless raw_config
      if path = @command.config_path
        if ex = Config::Path.new(path).expand_if_exists
          raw_config = File.open(ex){ |ff| Config::Raw.from_yaml(ff) }
        else
          raise ConfigError.new("Could not find config at #{path}") unless raw_config
        end
      elsif home = ENV["HOME"]?
        path = home + "/.config/loveline/loveline.yml"
        if ex = Config::Path.new(path).expand_if_exists
          raw_config = File.open(ex){ |ff| Config::Raw.from_yaml(ff) }
        else
          raw_config = Config::Raw::SAMPLE_DEFAULT
        end
      else
        raw_config = Config::Raw::SAMPLE_DEFAULT
      end
    end

    @command.config_options.each do |yaml|
      raw_config.merge! yaml
    end
    @config = Config.new(raw_config, input_tty: @input.tty?, output_tty: @output.tty?)
    @asker = if @config.interactive
      ->(var_name : String) do
        ll_print "Enter #{var_name}: "
        ll_gets.try(&.strip)
      end
    else
      ->(var_name : String) do
        nil.as String?
      end
    end
  end

  def go
    old_colorize_enabled = Colorize.enabled?
    Colorize.enabled = @config.color
    process(@command.subcmd)
  ensure
    Colorize.enabled = old_colorize_enabled.as Bool
  end

  def ll_print(*args, **kwargs)
    @output.print(*args, **kwargs)
  end

  def ll_puts(*args, **kwargs)
    @output.puts(*args, **kwargs)
  end

  def ll_gets(*args, **kwargs)
    @input.gets(*args, **kwargs)
  end
end
