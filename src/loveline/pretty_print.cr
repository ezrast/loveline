require "./cookie"

class Loveline
  def pretty_print_template(template)
    @output << "--- #{template.name} ---\n".colorize.light_blue
    pretty_print(
      method: template.method,
      address: template.address,
      headers: template.headers,
      cookies: template.cookies,
      variables: template.variables,
      body: template.body,
    )
  end

  def pretty_print_request(request : Request)
    @output << "--- dry run ---\n".colorize.light_yellow
    pretty_print(
      method: request.method,
      address: request.url.to_s,
      headers: request.headers,
      body: request.body,
      variables: {} of String => String,
      cookies: {} of String => String,
    )
  end

  def pretty_print_response(response : HTTP::Client::Response)
    heading = "#{response.status_code} #{response.status.to_s.gsub('_', ' ')}"
    @output << "--- ".colorize.cyan
    @output << case response.status_code
      when 100...200 then heading.colorize.white
      when 200...300 then heading.colorize.light_green
      when 300...400 then heading.colorize.green
      when 400...500 then heading.colorize.light_red
      when 500...600 then heading.colorize.red
      else heading.colorize.magenta
      end
    @output << " ---\n".colorize.cyan

    unless response.headers.empty?
      @output << "Headers:\n".colorize.light_magenta
      response.headers.to_a.sort_by{ |key, val| key }.each do |key, val|
        val.each do |vv|
          @output << "  " << key << ": ".colorize.yellow << vv << "\n"
        end
      end
    end

    # response body is handled in cmd_exec
  end

  def pretty_print(*,
    method : String?,
    address : String?,
    headers : Headers | HTTP::Headers,
    cookies : Hash(String, String),
    variables : Hash(String, String),
    body : String?
  )
    if method
      if address
        @output << method.colorize.cyan << " " << address << "\n"
      else
        @output << method.colorize.cyan << "\n"
      end
    elsif address
      @output << "... ".colorize.cyan << address << "\n"
    end

    case headers
    when Headers
      unless headers.empty?
        @output << "Headers:\n".colorize.yellow
        headers.sort_by(&.first).each do |pair|
          @output << "  " << pair.first << ": ".colorize.yellow << pair.last << "\n"
        end
      end
    when HTTP::Headers
      unless headers.empty?
        @output << "Headers:\n".colorize.yellow
        headers.each do |name, values|
          values.each do |value|
            @output << "  " << name << ": ".colorize.yellow << value << "\n"
          end
        end
      end
    end

    unless cookies.empty?
      @output << "Cookies:\n".colorize.light_magenta
      cookies.to_a.sort_by(&.first).each do |key, val|
        @output << "  " << key << ": ".colorize.yellow << val << "\n"
      end
    end

    unless variables.empty?
      @output << "Variables:\n".colorize.light_green
      variables.to_a.sort_by(&.first).each do |key, val|
        @output << "  " << key << ": ".colorize.yellow << val << "\n"
      end
    end

    pretty_print_body(body)
  end

  def pretty_print_body(body : String?)
    return unless body
    short_body = body[0..@config.truncate_body_after]

    if short_body.valid_encoding?
      if short_body.size != body.size
        @output << "Body (#{short_body.size} / #{body.size} chars):\n".colorize.light_red
      else
        @output << "Body:\n".colorize.light_red
      end
      @output << short_body << "\n"
    else
      if short_body.bytesize != body.bytesize
        @output << "Body (hex; #{short_body.bytesize} / #{body.bytesize} bytes):\n".colorize.light_red
      else
        @output << "Body (hex):\n".colorize.light_red
      end
      @output << hex_dump(short_body)
    end
  end

  BYTES_PER_LINE = 16
  def hex_dump(str : String) : Nil
    byte_count = 0
    str.bytes.each_slice(BYTES_PER_LINE) do |line|
      @output << byte_count.to_s(16).rjust(8, '0') << ": "
      byte_count += BYTES_PER_LINE
      line.each_slice(2) do |quartet|
        quartet.each{ |byte| @output << byte.to_s(16).rjust(2, '0') }
        @output << " "
      end
      @output << " "
      line.each_slice(2) do |quartet|
        quartet.each{ |byte| @output << ((32...128).includes?(byte) ? byte.chr : '.') }
      end
      @output << "\n"
    end
  end

  def pretty_print_cookie(cookie : Cookie, *, show_creation_time : Bool) : Nil
    @output << cookie.domain.colorize.cyan << cookie.path.colorize.light_cyan << ' '
    @output << cookie.name << ": ".colorize.yellow << cookie.value
    @output << " Created: #{cookie.creation_time.to_local.to_s("%Y-%m-%d %H:%M:%S")}".colorize.light_yellow if show_creation_time
    @output << "\n"
  end
end
