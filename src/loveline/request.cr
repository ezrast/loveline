require "crinja"
require "http"

require "./exceptions"
require "./interpolator"
require "./template"

class Loveline
  class Request
    enum TLS
      Secure
      Insecure
      None

      def context
        case self
        when Secure
          OpenSSL::SSL::Context::Client.new
        when Insecure
          OpenSSL::SSL::Context::Client.insecure
        else
          nil
        end
      end
    end

    getter method : String
    getter url : URI
    getter headers : HTTP::Headers
    getter body : String?
    getter tls : TLS

    def initialize(template : ExecutableTemplate, asker : Proc(String, String?), cookie_jar : CookieJar, insecure_tls : Bool, *, user_agent : String? = nil)
      interpolator = Interpolator.new(template.variables, asker)

      now = Time.utc
      uri_str = interpolator[
        template.address ||
        asker.call("address") ||
        raise(RequestNoAddress.new)
      ]
      raise RequestNoAddress.new if uri_str.empty?
      begin
        uri = URI.parse(uri_str)
        uri = URI.parse("http://" + uri_str) unless uri.scheme
      rescue err : URI::Error
        raise Loveline::AddressParseError.new(uri_str, err)
      end

      method = template.method.try{ |meth| interpolator[meth].upcase } || "GET"
      raise RequestNoMethod.new if method.empty?

      body = if tpl_body = template.body
        interpolator[tpl_body]
      end

      http_headers = HTTP::Headers.new
      template.headers.each do |pair|
        http_headers.add(interpolator[pair.first], interpolator[pair.last])
      end
      if user_agent && !http_headers.keys.find{ |key| key.downcase == "user-agent" }
        http_headers.add("User-Agent", USER_AGENT)
      end

      unless http_headers.includes?("Cookie")
        cookies = [] of {String, String}
        template.cookies.each do |key, val|
          key_interp = interpolator[key]
          val_interp = interpolator[val]
          cookies << {key_interp, val_interp}
        end

        request_host = http_headers.get?("Host").try(&.first) || uri.hostname || raise RequestInvalidHostname.new(uri_str)
        cookie_jar.search(
          request_host,
          uri.path,
          uri.scheme == "https",
          now,
          domain_matcher: Cookie::DomainMatcher::Request,
          path_matcher: Cookie::PathMatcher::Request
        ).sort_by!{ |cookie| {cookie.path.size, cookie.creation_time} }.each do |cookie|
          cookies << {cookie.name, cookie.value}
        end

        http_headers.add("Cookie", cookies.map{ |pair| "#{pair.first}=#{pair.last}" }.join("; ")) unless cookies.empty?
      end

      @tls = if uri.scheme == "https"
        if insecure_tls
          TLS::Insecure
        else
          TLS::Secure
        end
      else
        TLS::None
      end
      @method = method
      @url = uri
      @headers = http_headers
      @body = (body unless body.try(&.empty?))
    end

    def exec : HTTP::Client::Response
      HTTP::Client.new(uri: @url, tls: @tls.context) do |client|
        client.compress = false
        client.exec(
          method: @method,
          path: @url.path,
          headers: @headers,
          body: @body,
        )
      end
    rescue err : OpenSSL::SSL::Error
      raise Loveline::TlsError.new(err)
    rescue err
      raise Loveline::HttpExecError.new(err)
    end

    def python_escape(str : String)
      if str.includes? '\n'
        "'''\\\n#{str.gsub("\\", "\\\\").gsub("'", "\'")}'''"
      else
        "'#{str.gsub("\\", "\\\\").gsub("'", "\'")}'"
      end
    end

    def shell_escape(str : String)
      "'#{str.gsub(%('), %('"'"'))}'"
    end

    def render_crystal(io)
      ECR.embed("src/loveline/ecr/crystal.ecr", io)
    end

    def render_curl(io)
      default_method = @body ? "POST" : "GET"

      io << "curl "
      io << "-k " if @tls == TLS::Insecure
      io << "-X #{shell_escape(@method)} " unless @method == default_method
      io << shell_escape(@url.to_s)
      @headers.each do |key, vals|
        vals.each do |val|
          if val.empty?
            io << " \\\n  -H " << shell_escape(key) << ";"
          else
            io << " \\\n  -H " << shell_escape("#{key}: #{val}")
          end
        end
      end

      if body = @body
        highest = -1
        body.scan(/loveline_delim_(\d+)/) { highest = {highest, $1.to_i}.max }
        delim = "loveline_delim_#{highest + 1}"

        io << " \\\n " unless @headers.empty?
        io << " -d @- <<'" << delim << "'\n"
        io << body << "\n"
        io << delim
      end
      io << "\n"
    end

    def render_python_requests(io)
      ECR.embed("src/loveline/ecr/python-requests.ecr", io)
    end

    def render_jinja(io, jinja_path : String)
      begin
        jinja = File.read(jinja_path)
      rescue err : File::Error
        raise TemplateFileError.new(jinja_path, err)
      end

      begin
        output = Crinja.render(
          jinja,
          {
            "method" => @method,
            "url" => @url.to_s,
            "headers" => @headers.to_h,
            "body" => @body,
            "tls" => @tls.to_s.downcase,
          }
        )
      rescue err : Crinja::Error
        raise TemplateError.new(jinja_path, err)
      end
      io << output
    end
  end
end
