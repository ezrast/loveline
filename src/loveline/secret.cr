class Loveline
  # Loveline doesn't actually have any secrets management yet,
  # but we've made this class anyway no our interfaces don't change
  # drastically later.
  class Secret
    @val : String

    def initialize(@val)
    end

    # ONLY call this if you are about to place the result
    # * into another secret, or
    # * into an 0600 secrets directory, or
    # * onto the network as part of CmdExec
    def unwrap!
      @val
    end

    def to_s(*args, **kwargs)
      {% raise "Never to_s a secret!" %}
    end
  end
end
