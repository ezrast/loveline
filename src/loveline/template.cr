require "colorize"
require "yaml"

require "./laminatable_command"
require "./config"
require "./data_version"
require "./exceptions"
require "./secret"

class Loveline
  NAME_REGEX_STR = "[a-zA-Z0-9]+"
  NAME_REGEX = /\A#{NAME_REGEX_STR}\z/

  # https://tools.ietf.org/html/rfc2616#section-2.2
  COOKIE_NAME_REGEX = /\A[^\^\(\)<>@,;:\\"\/\[\]\?=\{\} \t]+\z/

  alias Headers = Array({String, String})

  # A template consists of all the info needed to make a request,
  # as well as variables that can be interpolated into header and
  # url strings.
  # Templates can be merged together, and thus used as "environments"
  # for requests.
  struct AnonTemplate
    include YAML::Serializable
    include YAML::Serializable::Strict

    DataVersion[tpl_v1]

    @[YAML::Field(emit_null: true)]
    getter address : String?
    @[YAML::Field(emit_null: true)]
    getter method : String?
    @[YAML::Field]
    getter headers : Loveline::Headers = Loveline::Headers.new
    @[YAML::Field]
    getter cookies : Hash(String, String) = {} of String => String

    @[YAML::Field]
    getter variables : Hash(String, String) = {} of String => String

    @[YAML::Field]
    getter body : String?

    def initialize(*,
      @address,
      @method,
      @headers,
      cookies,
      @variables,
      @body,
    )
      cookies.each_key do |key|
        raise BadCookieName.new(key) unless key =~ COOKIE_NAME_REGEX
      end
      @cookies = cookies
    end

    BLANK = new(
      address: nil,
      method: nil,
      headers: Headers.new,
      cookies: {} of String => String,
      variables: {} of String => String,
      body: nil,
    )
    def blank?
      return self == BLANK
    end
  end

  module ExecutableTemplate
    # Commented out types to quiet warnings
    # See https://github.com/crystal-lang/crystal/issues/8250
    abstract def name #: String
    abstract def address #: String?
    abstract def method #: String?
    abstract def headers #: Headers
    abstract def cookies #: Hash(String, String)
    abstract def variables #: Hash(String, String)

    abstract def blank? #: Bool

    def save(path : String, force : Bool)
      file_path = "#{path}/#{@name}"
      if File.exists?(file_path) && !force
        raise TemplateNameCollision.new(name, path)
      end

      File.open(file_path, "wb") do |ff|
        yaml(ff)
      end
    end
  end

  class Template
    include ExecutableTemplate

    getter name : String
    @inner : AnonTemplate

    def initialize(name : String, @inner : AnonTemplate)
      raise BadTemplateName.new(name) unless name =~ NAME_REGEX
      @name = name
    end

    def self.new(name : String, *args, **kwargs)
      new(name, AnonTemplate.new(*args, **kwargs))
    end

    def self.load(paths : Array(String), name : String)
      paths.each do |dir_path|
        file_path = "#{dir_path}/#{name}"
        if File.exists?(file_path)
          File.open(file_path, "rb") do |ff|
            return new(name, AnonTemplate.from_yaml ff)
          end
        end
      end
      raise NoSuchTemplate.new(name)
    end

    def self.delete(dir_path : String, name : String)
      file_path = "#{dir_path}/#{name}"
      if File.exists?(file_path)
        File.delete(file_path)
        return true
      end
      return false
    end

    def yaml(io : IO)
      @inner.to_yaml(io)
    end

    delegate :address, :method, :headers, :cookies, :variables, :body, to: @inner
    delegate :blank?, to: @inner
  end

  # A temporary template created from command line arguments
  class EphemeralTemplate
    include ExecutableTemplate

    @inner : AnonTemplate
    def name : String
      "<ephemeral>"
    end

    def initialize(@inner : AnonTemplate)
    end

    def self.from_command?(cmd : LaminatableCommand)
      headers = Headers.new
      cookies = {} of String => String
      variables = {} of String => String

      cmd.headers.each do |str|
        key, _, val = str.partition "="
        raise NoHeaderValue.new(str) if val.empty?
        headers << {key, val}
      end

      cmd.cookies.each do |str|
        key, sep, val = str.partition "="
        raise NoCookieValue.new(str) if sep.empty?
        cookies[key] = val
      end

      cmd.variables.each do |str|
        key, _, val = str.partition "="
        raise BadVariableName.new(key) unless key =~ NAME_REGEX
        raise NoVariableValue.new(str) if val.empty?
        variables[key] = val
      end

      ret = new(
        AnonTemplate.new(
          address: cmd.address,
          method: cmd.method,

          headers: headers,
          cookies: cookies,
          variables: variables,

          body: cmd.body,
        )
      )
      return ret unless ret.blank?
    end

    delegate :address, :method, :headers, :cookies, :variables, :body, to: @inner
    delegate :blank?, to: @inner
  end

  # A bunch of templates smooshed together.
  # Be careful of circular references.
  class LaminateTemplate
    include ExecutableTemplate

    def name : String
      @layers.map{ |tpl| tpl.name }.join ":"
    end

    def initialize(@layers : Indexable(ExecutableTemplate) = [] of ExecutableTemplate)
    end

    def address : String?
      @layers.reverse_each do |tpl|
        return tpl.address if tpl.address
      end
      return nil
    end

    def method : String?
      @layers.reverse_each do |tpl|
        return tpl.method if tpl.method
      end
      return nil
    end

    def headers : Headers
      ret = Headers.new

      @layers.each do |tpl|
        tpl.headers.each do |pair|
          ret << pair
        end
      end
      return ret
    end

    def cookies : Hash(String, String)
      ret = {} of String => String
      @layers.each do |tpl|
        ret.merge! tpl.cookies
      end
      return ret
    end

    def variables : Hash(String, String)
      ret = {} of String => String
      @layers.each do |tpl|
        ret.merge! tpl.variables
      end
      return ret
    end

    def body : String?
      @layers.reverse_each do |tpl|
        return tpl.body if tpl.body
      end
      return nil
    end

    def flatten(name)
      Template.new(
        name,
        address: address,
        method: method,

        headers: headers,
        cookies: cookies,
        variables: variables,

        body: body,
      )
    end

    def blank? : Bool
      @layers.all(&.blank?)
    end
  end
end
